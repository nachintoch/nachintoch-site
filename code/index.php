<?c
/**
 * Basado en los parámetros GET que reciba, muestra un spinner que da a elegir uno de
 * todos los lenguajes de programación disponibles para mostrar un código.
 * @author Manuel "Nachintoch" Castillo, contact@nachintoch.mx
 * @version 1.0, march 2017
 * @since Nachintoch.mx 1.4
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Nachintoch Desarrollos</title>
		<meta name="description" content="selector de lenguaje de programaci&oacute;n" />
		<meta name="keywords" content="codigo, programacion, java, c, sql, linux, web, ruby, php, mathlab, psudocode, racket, r" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head> 
	<body class="landing">
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
		<div id="main" class="wrapper style1">
			<div class="container">
				<? if(!isset($_GET["proyect"])) { ?>
				<h1>Selecciona un proyecto</h1>
				<select id="proy-sel" name="proy-sel" >
					<option value="default" selected >Seleccione un proyecto</option>
					<? 
					$dir = getcwd();
					$files = scandir($dir);
					for($i = 2; $i < count($files); $i++) {
						if($files[$i] == "index.php" || $files[$i] == "base_folder" || $files[$i] == "styler.php") {
							continue;
						}// se omite a sí mismo
						?> <option value="<? echo $files[$i] ?>" ><? echo $files[$i] ?></option><?
				} ?></select>
				<script type="text/javascript" >
				var selector = document.getElementById("proy-sel");
				function load_proy() {
					if(selector.value != "default")
						window.location = window.location +"?proyect=" +selector.value;
				}
				selector.addEventListener("change", load_proy, false);
				</script>
				<?} else {?>
				<h1>Selecciona un lenguaje de programaci&oacute;n</h1>
				<select id="leng-sel" name="leng-sel" >
					<option value="pseudo" selected >Pseudo-C&oacute;digo</option >
					<option value="c" >C</option >
					<option value="haskell" >Haskell</option >
					<option value="java" >Java</option >
					<option value="javascript" >Javascript</option >
					<option value="lua" >Lua</option>
					<option value="matlab" >MatLab</option >
					<option value="pascal" >Pascal</option >
					<option value="php" >PHP</option >
					<option value="python" >Python</option >
					<option value="r" >R</option >
					<option value="racket" >Racket</option >
					<option value="ruby" >Ruby</option >
					<option value="sql" >SQL</option >
					<option value="unix" >Comandos para sistemas POSIX-compatibles</option >
					<option value="windows" >Comandos para sistemas Windows</option >
				</select>
				<div id="display-container" style="width:100%;height:400px;margin-top:25px" >
					<input type="hidden" value="<? echo $_GET["proyect"]; ?>" name="proyect" id="proyect" />
					<div id="display" style="width:100%;height:100%" ></div>
				</div>
				<script type="text/javascript" src="/js/lenguaje_selector.js" ></script>
				<? } ?>
			</div>
		</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>