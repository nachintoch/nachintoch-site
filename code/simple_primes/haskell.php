<? require_once '../styler.php'; ?>
<pre>
<i>{- inicialmente debe ser invocado con el número que se quiere saber si es primo
   y 2 -}</i>
<b>is_prime :: Integer -> Integer -> Bool</b>
<b>is_prime n i</b> = <b>if</b> n &lt; 2 ||  <i>mod</i> n i == 0 <b>then False</b>
	<b>else if</b> i > <i>div</i> n 2 <b>then True</b>
	<b>else</b> is_prime n (i +1)
</pre>