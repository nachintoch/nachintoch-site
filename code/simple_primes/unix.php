<? require_once '../styler.php'; ?>
<pre>
<b>function</b> is_prime() {
	half=$((($1 /2) +1));
	<b>for</b> i <b>in</b> $(seq 2 $half)
	<b>do
		if</b> [ $(($1 % $i)) == 0 ]; <b>then</b>
			result=0;
			<b>return</b> 0;
		<b>fi
	done</b>
	result=1;
	<b>return</b> 1;
}
</pre>