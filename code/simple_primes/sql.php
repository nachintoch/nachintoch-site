<? require_once '../styler.php'; ?>
<pre>
<b>CREATE FUNCTION</b> is_prime(n INT) <b>RETURNS <i>INT</i>
BEGIN
	DECLARE</b> i <b><i>INT</i> DEFAULT</b> 2;
	search: <b>LOOP</b>
		<b>IF</b> MOD(n, i) = 0 <b>THEN</b>
			<b>RETURN</b> 0;
		<b>END IF</b>;
		<b>SET</b> i = i +1;
		<b>IF</b> i &lt;= n /2 <b>THEN</b>
			<b>ITERATE</b> search;
		<b>END IF</b>;
		<b>LEAVE</b> search;
	<b>END LOOP</b> search;
	<b>RETURN</b> 1;
<b>END</b>$$
</pre>