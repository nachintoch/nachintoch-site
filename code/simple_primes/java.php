<? require_once '../styler.php'; ?>
<pre>
	<b>public</b> <i>boolean</i> isPrime(<i>int</i> n) {
		<b>for</b>(<i>int</i> i = 2; i &lt;= n /2; i++) {
			<b>if</b>(n %i == 0) {
				<b>return false</b>;
			}
		}
		<b>return true</b>;
	}
</pre>