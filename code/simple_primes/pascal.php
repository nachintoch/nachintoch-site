<? require_once '../styler.php'; ?>
<pre>
<b>function</b> is_prime(n : <i>integer</i>): <i>boolean</i>;
<b>var</b>
	res : <i>boolean</i>; i : <i>integer</i>;
<b>begin</b>
	res := <b>true</b>;
	<b>for</b> i := 2 <b>to</b> n div 2 <b>do
	begin
		if</b> n <b>mod</b> i = 0 <b>then</b> res := <b>false</b>;
	<b>end</b>;
	is_prime := res;
<b>end</b>;
</pre>