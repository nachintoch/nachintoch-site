<? require_once '../styler.php'; ?>
<pre>
<b>function</b> is_prime($n) {
	<b>for</b>($i = 2; $i &lt;= $n /2; $i++) {
		<b>if</b>(!($n % $i)) <b>return false</b>;
	}
	<b>return true</b>;
}
</pre>