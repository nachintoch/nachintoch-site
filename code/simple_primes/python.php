<? require_once '../styler.php'; ?>
<pre>
<b>def</b> isprime(n):
	<b>for</b> i <b>in range</b>(2, n /2):
		<b>if not</b> n % i :
			<b>return <i>False</i></b>
	<b>return <i>True</i></b>
</pre>