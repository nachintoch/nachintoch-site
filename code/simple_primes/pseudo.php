<? require_once '../styler.php'; ?>
<pre>
is_prime(n)
Begin
	<b>FOR</b> i &lt;- 2 <b>TO</b> n /2 <b>DO</b>
		<b>IF</b> n <i>mod</i> i = 0 <b>THEN</b>
			<b>RETURN FALSE</b>
		<b>END IF</b>
	<b>END FOR</b>
	<b>RETURN TRUE</b>
End
</pre>