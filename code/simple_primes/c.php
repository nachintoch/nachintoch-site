<? require_once '../styler.php'; ?>
<pre>
<i>unsigned short int</i> is_prime(int n)
{
	<i>unsigned int</i> i;
	<b>for</b>(i = 2; i <= n /2; i++)
	{
		<b>if</b>(!(n % i))
		{
			<b>return</b> 0;
		}
	}
	<b>return</b> 1;
}
</pre>