<? require_once '../styler.php'; ?>
<pre>
<i>:isprime</i>
<b>SET</b> /A limit=%1 / 2
<b>FOR</b> /L %%i <b>IN</b> (2,1,%limit%) <b>DO</b> (
	<b>SET</b> /A aux=%1 %% %%i
	<b>IF</b> !aux! <b>EQU</b> 0 (
		<b>ECHO</b> FALSE
		<b>GOTO</b> <i>:EOF</i>
	)
)
<b>ECHO</b> TRUE
</pre>