<? require_once '../styler.php'; ?>
<pre>
<b>function</b> res = is_prime(n)
    <b>for</b> i = 2:(n /2)
        <b>if</b> ~mod(n, i)
            res = <i>false</i>;
            <b>return</b>;
        <b>end
    end</b>
    res = <i>true</i>;
<b>end</b>
</pre>