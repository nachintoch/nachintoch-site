<? require_once '../styler.php'; ?>
<pre>
(<b>define</b> (isprime n)
	(<b>let</b> ([res #t])
		(<b>for</b> ([i (<b>in-range</b> 2 (+ (/ n 2) 1))])
			(<b>when</b> (<b>equal?</b> (<b>modulo</b> n i) 0)
				(<b>set!</b> res #f)))
	res))
</pre>