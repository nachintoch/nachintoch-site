/*
 * Author: Manuel "Nachintoch" Castillo
 * http://sbcgamesdev.blogspot.mx/2015/04/phaser-tutorial-manage-different-screen.html
 * http://www.html5gamedevs.com/topic/5949-solution-scaling-for-multiple-devicesresolution-and-screens/
 * Version: 1.0, june 2016
 */

var ABSOLUTE_WIDTH = 800;
var ABSOLUTE_HEIGHT = 600;

 // construye el juego y mantiene una referencia "game" a él.
var game = new Phaser.Game(ABSOLUTE_WIDTH, ABSOLUTE_HEIGHT, Phaser.AUTO, 'game', {preload : preload,
	create : create, update : update});

var bckgnd;

// inicio de cosa rara

var windowWidth;
var windowHeight;
var defaultGameHeight;
var maxGameWidth;
var maxGameHeight;
var gameWidth;
var gameHeight;
var scaleX;
var scaleY;
var offsetX;
var offsetY;
var LANDSCAPE = 0;
var PORTRAIT = 1;

var screenDims = calculateScreenMetrics(ABSOLUTE_WIDTH, ABSOLUTE_HEIGHT, LANDSCAPE);

function calculateScreenMetrics(aDefaultWidth, aDefaultHeight,
		aOrientation = LANDSCAPE, aMaxGameWidth = undefined,
		aMaxGameHeight = undefined) {
	windowWidth = window.innerWidth;
	windowHeight = window.innerHeight;
	if((windowWidth < windowHeight && aOrientation == LANDSCAPE) ||
		(windowHeight < windowWidth && aOrientation == PORTRAIT)) {
		var tmp = windowWidth;
		windowWidth = windowHeight;
		windowHeight = tmp;
	}//intercambia si la orientación no concuerda
	if(aMaxGameWidth === undefined || aMaxGameHeight === undefined) {
		if(aOrientation == LANDSCAPE) {
			aMaxGameWidth = Math.round(aDefaultWidth *1420 /1280);
			aMaxGameHeight = Math.round(aDefaultHeight *960 /800);
		} else {
			aMaxGameWidth = Math.round(aDefaultWidth *960 /800);
			aMaxGameHeight = Math.round(aDefaultHeight *1420 /1280);
		}//asigna dimensiones máximas dependiendo la orientación deseada
	}//si las dimensiones máximas no están asignadas
	var defaultAspect = aOrientation == LANDSCAPE ? 1280 : 800;
	var windowAspect = windowWidth /windowHeight;
	offsetX = 0;
	offsetY = 0;
	gameWidth = 0;
	gameHeight = 0;
	if(windowAspect > defaultAspect) {
		gameHeight = aDefaultHeight;
		gameWidth = Math.ceil((gameHeight *windowAspect) /2.0) *2;
		gameWidth = Math.min(gameWidth, aMaxGameWidth);
		offsetX = (gameWidth -aDefaultWidth) /2;
		offsetY = 0;
	} else {
		gameWidth = aDefaultWidth;
		gameHeight = Math.ceil((gameWidth /windowAspect) /2.0) *2;
		gameHeight = Math.min(gameHeight, aMaxGameHeight);
		offsetX = 0;
		offsetY = (gameHeight -aDefaultHeight) /2;
	}//ajusta el aspecto
	scaleX = windowWidth /gameWidth;
	scaleY = windowHeight /gameHeight;
	defaultGameWidth = aDefaultWidth;
	defaultGameHeight = aDefaultHeight;
	maxGameWidth = aMaxGameWidth;
	maxGameHegiht = aMaxGameHeight;
}//calculateScreenMetrics

// final de cosa rara

// declaración de funciones para implementar al nivel

/*
 * Define un conjunto de instrucciones a ejecutar mientras se carga el juego.
 * 
 * De acuerdo a la filosofía de desarrollo de videojuegos basados en web con
 * Phaser
 *
 * returns undefined
 */
function preload() {
	// cargamos los sprites
	game.load.image('background', 'img/prueba.png');
}//preload

/*
 * Crea los elementos del juego y define su comportamiento.
 *
 * De acuerdo a la filosofía de desarrollo de videojuegos basados en web con
 * Phaser
 *
 * returns undefined
 */
function create() {
	game.input.maxPointers = 1;
	game.stage.disableVisibilityChange = false;
	game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
	game.scale.setUserScale(scaleX, scaleY);
	game.scale.pageAlignHorizontally = true;
	game.scale.pageAlignVertically = true;
	if(!game.device.desktop) {
		this.scale.forceOrientation(true, false);
	}//si es móvil...?
	game.stage.backgroundColor = 0x8080FF;
	// carga el fondo del escenario
	bkgnd = game.add.sprite(game.world.centerX, game.world.centerY,
		'background');
	bkgnd.anchor.setTo(0.5, 0.5);
}//create

/*
 * Define el comportamiento del juego para interactuar con el usaurio sobre el
 * tiempo.
 *
 * De acuerdo a la filosofía de desarrollo de videojuegos basados en web con
 * Phaser
 *
 * returns undefined
 */
function update() {
	
}//update
