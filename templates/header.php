<?
/**
 * Define un encabezado común para todas las páginas del sitio. No incluye las
 * etiquetas html, head, title ni las meta etiquetas de descripción y palabras clave
 * para permitirle a cada página definirlas a necesidad.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!--
	Landed by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta charset="UTF-8" />
	<meta name="author" content="Manuel Nachintoch Castillo" />
	<!--[if lte IE 8]><script src="/css/ie/html5shiv.js"></script><![endif]-->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"
  	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  	crossorigin="anonymous"></script>
	<script src="/js/jquery.scrolly.min.js"></script>
	<script src="/js/jquery.dropotron.min.js"></script>
	<script src="/js/jquery.scrollex.min.js"></script>
	<script src="/js/skel.min.js"></script>
	<script src="/js/skel-layers.min.js"></script>
	<script src="/js/init.js"></script>
	<noscript>
		<link rel="stylesheet" href="/css/skel.css" />
		<link rel="stylesheet" href="/css/style.css" />
		<link rel="stylesheet" href="/css/style-xlarge.css" />
	</noscript>
	<link rel="icon" type="image/png" href="/images/favicon.ico"/>
	<!--link rel="alternate" href="http://www.nachintoch.mx" hreflang="es-MX" /-->
	<!--[if lte IE 9]><link rel="stylesheet" href="/css/ie/v9.css" /><![endif]-->
	<!--[if lte IE 8]><link rel="stylesheet" href="/css/ie/v8.css" /><![endif]-->
