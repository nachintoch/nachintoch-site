<?
/**
 * Menú de navegación presente en todas las páginas de la web.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<header id="header">
	<noscript >
	Tu navegador no soporta javascript, o lo has desactivado. Por favor,
	activa javascript o usa un navegador soportado.
	</noscript>
	<h1 id="logo">Nachintoch Desarrollos</h1><br/>
	<nav id="nav">
		<ul>
			<li><a href="/">Inicio</a></li>
			<li>
				<a href="">Contenido</a>
				<ul>
					<li><a href="/libraries">C&oacute;digo y bibliotecas</a></li>
					<li><a href="/teaching" >Notas de clase</a></li>
                    <li><a href="/research" >
                        Art&iacute;culos e investigaci&oacute;n</a></li>
					<!--li><a href="/geek_calendar" >Efem&eacute;rides nerdas</a></li-->
					<li><a href="/about.php">Acerca de</a></li>
					<!-- No quitar el .php o lo confunde con el de Google .xml -->
					<li><a href="/sitemap.php" >Mapa de sitio</a></li>
				</ul>
			</li>
			<li>
				<a href="" >Proyectos</a>
				<ul>
					<li><a href="/open_android_apps">Apps Libres para Android</a></li>
					<li><a href="/projects">Open Hardware</a></li>
					<li><a href="/games.php">Juegos</a></li>
				</ul>
			</li>
		</ul>
	</nav>
</header>
