<?
/**
 * Define el pie de página de todas las páginas del sitio.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<footer id="footer">
	<ul class="icons">
		<li><a href="http://nachintoch.wordpress.com/"
			class="icon alt fa-wordpress"
			onclick="return !window.open(this.href, 'pop');" >
			<span class="label">WordPress</span></a></li>
		<li><a href="mailto:contact@nachintoch.x10.mx"
			class="icon alt fa-envelope" >
			<span class="label">Email</span></a></li>
			<li><a href="https://gitlab.com/nachintoch"
				class="icon alt fa-git-square"
				onclick="return !window.open(this.href, 'pop');" >
				<span class="label">GitLab</span></a></li>
		<li><a href="https://github.com/Nachintoch"
			class="icon alt fa-github"
			onclick="return !window.open(this.href, 'pop');" >
			<span class="label">GitHub</span></a></li>
		<li><a href="https://bitbucket.org/nachintoch"
			class="icon alt fa-bitbucket"
			onclick="return !window.open(this.href, 'pop');" >
			<span class="label" >BitBucket</span></a></li>
		<li><a href="/about.php" class="icon alt fa-info-circle">
			<span class="label" >&iquest;Quienes somos?</span></a></li>
	</ul>
	<ul class="copyright">
		<li>Nachintoch 2015 - <?= date("Y") ?>. El contenido de esta web puede ser
			redistribuido sin necesidad de consultar al propietario,
			siempre que haga una cita de la URL del mismo</li>
		<li>Plantilla de sitio HTML5: Landed by <a href="http://html5up.net">HTML5 UP</a>,
			bajo el patrocinio de <a href="https://x10hosting.com" >X10 Free hostings</a></li>
	</ul>
</footer>
