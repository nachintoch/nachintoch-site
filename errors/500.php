<!DOCTYPE HTML>
<html>
	<head>
		<title>e101 500 - Error del servidor</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Error 500 Error interno del servidor" />
		<meta name="keywords" content="" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php' ?>
	</head>
	<body>

		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php'; ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>e101 - La hemos liado</h2>
						<p>Muchas veces nos gusta hablar mal del usuario por pedirles su nombre y obtener
						como respuesta qu&eacute; comieron ayer. Pero esta vez la culpa <i>ist nostra</i>.
						Te agradecer&iacute;a mucho que 
						<a href="mailto:contact@nacintoch.mx">reportaras a contact@nachintoch.mx</a> qu&eacute;
						estabas haciendo, que como consecuencia, nuestro mareado y parchado servidor te
						mostr&oacute; est&aacute; p&aacute;gina de error que espero nadie lea.<br/>
						Disculpa el inconveniente :P</p>
					</header>
		<!-- Footer -->
			<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html> 
