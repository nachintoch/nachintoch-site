<!DOCTYPE HTML>
<html>
	<head>
		<title>e101 403 - Acceso denegado</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Error 403 Acceso denegado" />
		<meta name="keywords" content="" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php' ?>
	</head>
	<body>

		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php'; ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>e101 - Lo siento, me temo que no puedo hacer eso</h2>
						<p>Parece que has intentado usar un protocolo que he prohib&iacute;do en el
						servidor, o acceder a un recurso que por alguna raz&oacute;n decid&iacute; no
						publicar a los cuatro vientos. Puedes encontrar el código fuente de todo este
						sitio web en:
						<a href="https://gitlab.com/nachintoch/nachintoch-site">https://gitlab.com/nachintoch/nachintoch-site</a>
						.</p>
					</header>
		<!-- Footer -->
			<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html>
