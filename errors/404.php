<!DOCTYPE HTML>
<html>
	<head>
		<title>e101 404 - P&aacute;gina no encontrada</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Error 404 - P&aacute;gina no encontrada" />
		<meta name="keywords" content="" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php' ?>
	</head>
	<body>

		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php'; ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>e101 - Parece que has encontrado un Link muerto</h2>
						<p>No s&eacute; c&oacute;mo habr&aacute;s llegado hasta aqu&iacute;, pero si diste click
						en un Link roto, te agradecer&iacute;a mucho
						<a href="mailto:contact@nacintoch.mx">lo reportaras a contact@nachintoch.mx</a>.
						Y disculpa el inconveniente :P</p>
					</header>
		<!-- Footer -->
			<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html>