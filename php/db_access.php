<?
/*
 * Provee acceso a la base de datos
 * @author Manuel "Nachintoch" Castillo contact@nacintoch.mx
 * @versión 1.0, octubre 2013
 */
define("HOST", "localhost");
define("USER", "donkeykong");
define("PASSWORD", "its_a_secret_to_everybody");
define("DATABASE", "nachinto_geek_calendar");

$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
