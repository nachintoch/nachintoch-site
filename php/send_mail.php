<?
/**
 * Realiza el envío de correo a la dirección solicitada
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
if(mail("contact@nachintoch.mx", "Donation interest!", "Send by "
	.$_POST['email'] .", automatically from donations form. User said: "
	.$_POST['mssg'])) {
	echo "OK";
} else {
	header('HTTP/1.1 500 Internal Server Error');
	echo "Cannot send email";
}//responde 
