<?
/**
 * Basandose en el día de consulta, comrpueba si hay una efeméride para dicho día.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
// asigna español como idioma local
setlocale(LC_ALL, 'es_ES');
// indica la zona horaria del servidor
date_default_timezone_set('America/Mexico_City');
require_once 'db_access.php';
if(date('z') +1 == 256 && chances_for_nonconstant_dates()) {
	echo "geek_calendar/ada.jpg\" alt=\"&iexcl;Feliz d&iacute;a del programador!\" title=\"&iexcl;Feliz d&iacute;a del programador!";
	return;
}//día del programador!
if(date('d-M-Y') == date("d-M-Y", strtotime("first friday of august")) &&
		chances_for_nonconstant_dates()) {
	echo "geek_calendar/duff.jpg\" alt=\"Cerveza de la d&iacute;a *hip!*\" title=\"Cerveza de la d&iacute;a *hip!*";
	return;
}// día de la cerveza
$month = date('m');
$day = date('d');
$result = 'digital-brain.jpg" alt="Los afros son la onda" title="Los afros son la onda';
$img = $title = $url = NULL;
if($stmt = $mysqli -> prepare("SELECT img, title, url FROM dates WHERE MONTH(eventDate) = ? AND DAY(eventDate) = ?")) {
	$stmt -> bind_param('ii', $month, $day);
	$stmt -> execute();
	$stmt -> store_result();
	$stmt -> bind_result($img, $title, $url);
	require_once 'Mobile_Detect.php';
	$mov_det = new Mobile_Detect();
	while($stmt -> fetch()) {
		if(1 /($stmt -> num_rows) >= (mt_rand(0, mt_getrandmax() -1) /mt_getrandmax())) {
			break;
		}//decide cual mostrar
	}//explora los resultados
	if(isset($img, $title)) {
		$result = "geek_calendar/$img\" alt=\"$title\" title=\"$title";
		if(isset($url)) {
			$result .= "\" style=\"cursor:pointer\" onclick=\"window.location='$url'";
			if($mov_det -> isMobile()) {
				$result .= ";alert(this.title)";
			}//si es dispositivo móvil pone un alert con el title
		} else {
			if($mov_det -> isMobile()) {
				$result .= "\" onclick=\"alert(this.title)";
			}//si es dispositivo móvil pone un alert con el title
		}//si url también está definido
	}//construye el resultado*/
	echo $result;
} else {
	echo $result;
}//si pudo hacer el query o no

function chances_for_nonconstant_dates() {
	 return 0.5 >= (mt_rand(0, mt_getrandmax() -1) /mt_getrandmax());
}
