<?
/*
 * Página de inicio del sitio.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Nachintoch Desarrollos</title>
		<meta name="description" content="Fomentando el Software Libre y la cultura geek" />
		<meta name="keywords" content="codigo, android, open, source, software, libre, programacion, computacion, ingenieria, hardware" />
		<? require_once 'templates/header.php'; ?>
	</head>
	<body class="landing">
		<!-- Header -->
		<? require_once 'templates/menu.php' ?>
		<!-- Banner -->
			<section id="banner">
				<div class="content">
					<header>
						<h2 title=" - S&oacute;crates" >El conocimiento nos har&aacute;
							libres.</h2>
						<p>Crear, compartir y mejorar nuestra vida a trav&eacute;s del
							conocimiento<br/> y sus aplicaciones, amplia el panorama de
							posibilidades;<br/> creando un mundo donde el l&iacute;mite es la
							imaginaci&oacute;n.</p>
					</header>
					<span class="image" >
						<img src="images/<? require_once 'php/day_event_checker.php' ?>"
						onload="this.parentElement.title=this.title;this.parentElement.onclick=this.onclick;this.parentElement.style.cursor=this.style.cursor" />
					</span>
				</div>
				<a href="#one" class="goto-next scrolly">Next</a>
			</section>

		<!-- One -->
			<section id="one" class="spotlight style1 bottom">
				<span class="image fit main"><img src="images/circuit-board.jpg" alt="" /></span>
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="4u 12u$(medium)">
								<header>
									<h2>Fomentando el <i title="Open Source = C&oacute;digo Aberto" >
										Open Source</i>; &iquest;lo conoces?</h2>
									<p>Es m&aacute;s que software gratis. Es
										corregir, mejorar y crear tu propia tecnolog&iacute;a
										con tus propias manos.</p>
								</header>
							</div>
							<div class="4u 12u$(medium)">
								<p><u title="De acuerdo con la UNESCO" >El derecho a la
									educaci&oacute;n es un derecho humano fundamental</u>; pero no
									solamente porque alguien o algo "importante" lo dijo y
									promulg&oacute; en alguna parte. Es fundamental porque nos
									permite entender y manipular el entorno que nos rodea; con el
									objetivo de mejorar nuestra calidad de vida: hacer nuestra
									vida mejor y "m&aacute;s f&aacute;cil".</p>
							</div>
							<div class="4u$ 12u$(medium)">
								<p><i>¿Qu&eacute; tiene que ver el derecho a la educaci&oacute;n
									con el Open Source?</i> Si solo usamos el
									<i title="Software Libre = Open Source" >Software Libre</i>
									como una alternativa al software propietario; no mucho
									quiz&aacute;. Pero si lo usamos como los cimientos para crear,
									compartir y mejorar; a&uacute;n si no sabes programar,
									aprender sobre estas tecnolog&iacute;as es un potente aditivo
									para abrir nuestras posibilidades hasta los l&iacute;mites de
									la imaginaci&oacute;n y la creatividad en sus diferentes
									expresiones.</p>
							</div>
						</div>
					</div>
				</div>
				<a href="#two" class="goto-next scrolly"></a>
			</section>

		<!-- Two -->
			<section id="two" class="spotlight style2 right">
				<span class="image fit main"><img src="images/c-code.jpg" alt="" /></span>
				<div class="content">
					<header>
						<h2>Todos podemos compartir y aprender de los dem&aacute;s.</h2>
						<p>Las peque&ntilde;as ideas y los proyectos personales pueden
							contener; desde peque&ntilde;os <i>"truquitos"</i> y estrategias
							interesantes, hasta tener utilidad en campos en los que no lo
							esperar&iacute;amos.</p>
					</header>
					<p>Adem&aacute;s, contar con repositorios donde mantengamos nuestros
						proyectos e intereses, puede usarse como un portafolio que podemos
						usar para evidenciar nuestras virtudes y capacidades; cosa
						&uacute;til para conseguir un buen trabajo.</p>
					<ul class="actions">
						<li><a href="libraries" class="button">Peque&ntilde;os ejemplos</a></li>
					</ul>
				</div>
				<a href="#three" class="goto-next scrolly"></a>
			</section>

		<!-- Three -->
			<section id="three" class="spotlight style3 left">
				<span class="image fit main bottom"><img src="images/world-is-open.jpg" alt="" /></span>
				<div class="content">
					<header>
						<h2>Los invito a compartir y aprovechar los recursos que concentro
							en esta web.</h2>
						<p title="En la Facultad de Ciencias de la UNAM" >Pongo a su
							disposici&oacute;n las notas de clase, ejemplos y otros materiales
							de los distintos cursos de licenciatura en los que he participado
							como acad&eacute;mico.</p>
					</header>
					<p>En mi blog podr&aacute;n encontrar peque&ntilde;os proyectos en los
						que he participado o he desarrollado por mi cuenta; junto con varios
						tutoriales y gu&iacute;as sobre distintas tecnolog&iacute;as
						digitales; como Android y Linux.</p>
					<ul class="actions">
						<li><a href="teaching" class="button">Quiero ver las notas</a></li>
						<li><a href="http://nachintoch.wordpress.com" class="button">Quiero ver el blog</a></li>
						<li>Y proximanente m&aacute;s recursos</li>
					</ul>
				</div>
			</section>
		<!-- Footer -->
		<? require_once 'templates/footer.php' ?>
	</body>
</html>
