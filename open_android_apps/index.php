<?
/**
 * Página que presenta las aplicaciones Android no publicadas en Google Play.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, september 2016
 * @since Nachintoch.mx 1.0, september 2016
 */
?>
<html>
	<head>
		<title>Aplicaciones para Android - Nachintoch Desarrollos</title>
		<meta name="description" content="Nachintoch - Aplicaciones software libre para Android" />
		<meta name="keywords" content="app, aplicacion, android" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Aplicaciones software libre para Android</h2>
						<p>public Activity</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Aqu&iacute; podr&aacute; encontrar aplicaciones para android que pueden servir para aprender
							a trabajar en dicha plataforma, o que simplemente puede incorporar a sus proyectos.</p>
							<p>Cuento con m&aacute;s aplicaciones gratis; m&aacute;s no software libre, para Android.
							Estas podr&aacute; encontrarlas en la  <a href="https://play.google.com/store/search?q=nachintoch" >
								Google Play Store</a>.</p>
							<hr/>
							<p>Puesto que estas aplicaciones no ser&aacute;n identificadas como de origen por la Google Play
							por su dispositivo (salvo que usa alguna versi&oacute;n no oficial de Android), debe realizar
							el siguiente procedimiento para poder instalarlas:<br/>
							<ol><li>Primero debemos abrir los ajustes del dispositivo.
								<li>HINT: En la mayoría de los dispositivos con JB (Jelly Bean) o superior, podemos
									acceder a los ajustes desplegando la barra de notificaciones, dónde en la parte
									superior hay un ícono con herramientas (un desarmador y llave de tuercas o un
									engrane) que al tocarlo nos lleva a los ajustes. Normalmente también hay un
									acceso en el menú de aplicaciones del dispositivo.</li>
								<li>Ahora, en los ajustes, debemos seleccionar la opción "Seguridad" de entre todas
									las opciones listadas. Dentro de seguridad, buscamos la opción "Orígenes 
									desconocidos" y debemos asegurarnos que la casilla correspondiente está marcada.</li>
								<li>De lo contrario, damos un toque para que sea así. Al tratar de marcar la
									casilla, nos aparecerá un diálogo que nos explica que permitir la instalación
									de cualquier aplicación de origen desconocido, representa un riesgo de
									seguridad. Tocamos "OK" para asumir el riesgo y continuar.</li></ol></p>
							Nunca conf&ieacute; en un software para el que no tiene el c&oacute;digo fuente ;)
							<hr/>
							<table><tbody><tr><td><h3><a href="../libraries/android/sensor_listener_lib_v1.1.jar" >
								Biblioteca/ejemplo para uso y manejo de sensores</a></h3>
								<p>Este no es propiamente una biblioteca, es m&aacute;s bien una biblioteca que puede usar
								como apoyo para aprender a manejar y usar sensores en Android; o bien, que puede simplemente
								usar en sus proyectos si as&iacute; lo considera.</p>
								<p>Puede disponer del c&oacute;digo fuente aqu&iacute;:
									<a href="https://github.com/Nachintoch/android-sensor-listener" target="_blank" >
										https://github.com/Nachintoch/android-sensor-listener</a></p>
								</td></tr>
							<!--tr><td><h3><a href="" >Ejemplo de uso de la bilioteca de sensores</a></h3>
							
							</td></tr-->
							<!--tr><td><h3><a href="" ></a></h3></td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>x
	</body>
</html>