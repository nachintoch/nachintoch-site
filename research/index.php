<?
/**
 * Página que presenta la lista de artículos publicados.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.x10.mx
 * @version 1.0, march 2024
 * @since Nachintoch.mx 1.1, march 2024
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Art&iacute;culos e investigaci&oacute;n</title>
		<meta name="description" content="Artículos de investigación en los que he participado - conocimiento al alcance de todos" />
		<meta name="keywords" content="ingenieria, software, desarrollo, centrado, usuario, calidad, proceso" />
        <? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Art&iacute;culos e investigaci&oacute;n</h2>
						<p>Ingenier&iacute;a de software - Calidad de procesos y
                        productos de software</p>
					</header>
                    <div class="row 150%">
						<section id="content">
                            <p>Desde hace ya varios a&ntilde;os colaboro con un
                            grupo de investigaci&oacute;n en el Instituto de
                            Ciencias Aplicadas y Tecnolog&iacute;a UNAM, en 2022
                            el grupo me apoyo para participar en un congreso de
                            Ingenier&iacute;a de Software del que deriv&oacute;
                            un art&iacute;lo, dando comienzo a mi
                            participaci&oacute;n en la investigaci&oacute;n
                            respecto a la calidad de procesos y productos de
                            software.</p>
                            <p>Actualmente continuo trabajando en futuras
                            publicaciones como parte de mi proyecto doctoral. En
                            el futuro planeo continuar con esta trayectoria.</p>
                            <p>En esta p&aacute;gina presento enlaces a los
                            art&iacute;los que he publicado hasta ahora
                            esperando que sean de su inter&eacute;s:</p>
                            <table><tbody>
                                <tr><td>
                                    <h3><a target="_blank"
                                        href="https://aircconline.com/abstract/ijsea/v14n5/14523ijsea01.html" >
                                        Agile, User-Centered Design and Quality
                                        in Software Processes for Mobile
                                        Application Development Teaching
                                    </a></h3>
                                    <p style="font-weight:bold" >Resumen</p>
                                    <p>Se han explorado m&eacute;todos
                                    &Aacute;giles en cursos de Ingenier&iacute;a
                                    de Software con la intenci&oacute;n de
                                    cerrar la brecha entre la industria y los
                                    perfiles profesionales. Hemos estructurado
                                    un curso de desarrollo de aplicaciones
                                    Android con base en un proceso &Aacute;gil
                                    centrado en el usuario para el desarrollo de
                                    herramientas educacionales digitales. Este
                                    proceso se basa en Scrum y Extreme
                                    Programming en combinaci&oacute;n con
                                    enfoques de Experiencia del Usuario (UX). El
                                    curso ocurre en dos fases: durante la
                                    primera mitad del semestre se presenta
                                    teoría y el desarrollo de aplicaciones
                                    Android, la segunda mitad se trabaja como un
                                    taller en el que los estudiantes desarrollan
                                    para un cliente real. La introducci&oacute;n
                                    de UX y el dise&ntilde;o centrado en el
                                    usuario explotando la cercana
                                    relaci&oacute;n con interesados que se
                                    espera en m&eacute;todos &Aacute;giles
                                    permite el desarrollo de diferentes
                                    características de calidad. Desde 2019 dos
                                    de los proyectos han sido extendidos y otro
                                    ha sido desarrollado empleando el proceso
                                    descrito y ex-alumnos del curso. Tanto
                                    estudiantes como interesados (stakeholders)
                                    encuentran valor en los productos generados
                                    y en el proceso. (El art&iacute;culo
                                    est&aacute; en ingl&eacute;s).</p>
                                </td></tr>
                                <!--tr><td><h3><a href="" ></a></h3></td></tr-->
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        </div>
		<!-- Footer -->
        <? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
