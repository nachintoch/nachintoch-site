<?
/**
 * Índice de archivos por default personalizado para páginas aún en contrucción...
 * Como todas...
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */

// asigna español como idioma local
setlocale(LC_ALL, 'es_ES');
// indica la zona horaria del servidor
date_default_timezone_set('America/Mexico_City');
 
// contiene el directorio actual
$dir = getcwd();

// lista de archivos del directorio. Los dos primeros son . y ..
$files = scandir($dir);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>&Iacute;ndice de <? echo basename($dir); ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="&Iacute;ndice de archivos" />
		<meta name="keywords" content="" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php' ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php'; ?>
		<!-- Main -->
		<div id="main" class="wrapper style1">
			<div class="container">
				<header class="major">
					<h2>&Iacute;ndice de contenido</h2>
					<p>P&aacute;gina en construcci&oacute;n.<br/>
					&iexcl;Hola! Lamento ofrecerte de esta manera tan escueta el contenido de
					esta secci&oacute;n del sitio, pero esta p&aacute;gina se encuentra a&uacute;n
					en construcci&oacute;n. Proximamente, este mensaje ser&aacute; sustituido por
					el contenido real de la p&aacute;gina.</p>
				</header>
				<!-- Text -->
				<section >
					<p>A continuaci&oacute;n, una lista de los archivos hospedados en esta
					secci&oacute;n del sitio:</p>
					<section>
						<div class="table-wrapper">
							<table>
								<thead>
									<tr>
										<th></th>
										<th>Nombre</th>
										<th>&Uacute;ltima modificaci&oacute;n</th>
										<th>Tama&ntilde;o</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1.</td>
										<td><a href=".." >Volver al directorio padre</a></td>
										<td><? echo htmlentities(strftime("%A %e, %B %Y, %k:%M", filemtime("..")),
											ENT_SUBSTITUTE, "ISO-8859-15") ?></td>
										<td>Directorio</td>
									</tr>
									<?
									for($i = 2; $i < count($files); $i++) {
										if($files[$i] == "index.php") {
											continue;
										}// se omite a sí mismo
										echo "<tr><td>" .$i .".</td><td><a href='" .$files[$i] ."' >" .$files[$i]
											."</a></td><td>"
											.htmlentities(strftime("%A %e, %B %Y, %k:%M", filemtime($files[$i])),
												ENT_SUBSTITUTE, "ISO-8859-15")
											."</td>";
										if(is_dir($files[$i])) {
											echo "<td>Directorio</td>";
										} else {
											$file_size = filesize($files[$i]);
											if($file_size > 1024) {
												$file_size /= 1024;
											} else {
												$file_size = round($file_size, 2);
												$file_size .= " bytes";
											} if(is_numeric($file_size) && $file_size > 1024) {
												$file_size /= 1024;
											} else if(is_numeric($file_size)) {
												$file_size = round($file_size, 2);
												$file_size .= " Kb";
											} if(is_numeric($file_size) && $file_size > 1024) {
												$file_size /= 1024;
											} else if(is_numeric($file_size)){
												$file_size = round($file_size, 2);
												$file_size .= " Mb";
											}//recorre el tamaño
											if(is_numeric($file_size)) {
												$file_size = round($file_size, 2);
												$file_size .= " Gb";
											}//si estuvo diviendo hasta Gb
											echo "<td>" .$file_size ."</td>";
										}//mide su tamaño o dice que es un directorio
										echo "</tr>";
									}//recorre todos los archivos y directorios hijos
									?>
								</tbody>
							</table>
						</div>
					</section>
				</section>
			</div>
		</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html> 
 
 
