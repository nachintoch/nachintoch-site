<!DOCTYPE HTML>
<html>
	<head>
		<title>e101 404 - P&aacute;gina no encontrada</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Error 404 P&aacute;gina no encontrada" />
		<meta name="keywords" content="" />
		<meta http-equiv="refresh" Content="8; URL=index" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php' ?>
	</head>
	<body>

		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php'; ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>e101 - Parece que has encontrado un Link muerto</h2>
						<p>Seguramente llegaste aqu&iacute; debido a un link antig&uuml;o.
						Cuando el contenido de las notas empez&oacute; a crecer, decid&iacute;
						reorganizarlo por asignatura.<br/>
						En un momento ser&aacute;s redirigido al &iacute;ndice de las notas de
						clase. Si nada sucede, por favor, da click <a href="index" >aqu&iacute;.</a></p>
					</header>
		<!-- Footer -->
			<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html>