//package mx.nachintoch.teaching;

/**
 * <p>Imeplementaci&oacute;n al problema de la M&aacute;xima Subsecuencia
 * Consecutiva (Maximum Consecutive Subsequence, MCS).</p>
 * Para realizar una prueba de este programa se debe hacer una llamada como
 * la siguiente en consola: (recuerde agregar el paquete de quitarle el
 * comentario a la primera linea)
 * <br>java MCS 2 -3 1.5 -1 3 -2 -3.3</br>
 * <p>Esto debe resultar en los siguientes mensajes:
 * <br>La m&aacute;xima subsecuencia consecutiva suma: 3.5</br>
 * <br>Y va de los &iacute;ndices 2 al 4, es decir:</br>
 * <br>1.5, -1, 3, </br></p>
 * @author <a href="mailto:teshanatsch@gmail.com" >Manuel "Nachintoch"
 * Castillo</a>
 * @version 1.1, october 2015
 * @since august 2015
 */
public class MCS {

	// método main

	/**
	 * Toda la funcionalidad de la clase, que &uacute;nicamente resuelve el
	 * problema MCS, est&aacute; escrita en el main. Esto es una mala
	 * pr&aacute;ctica de programaci&oacute;n, pero puesto que la funcionalidad
	 * de clase clase en m&iacute;nima, me lo he permitido.
	 * @param args - Arreglo de cadenas que contiene en cada localidad un
	 * n&uacute;mero "real" por precondición de este algoritmo.
	 * @since MCS 1.1, october 2015
	 */
	public static void main(String[] args) {
		double globalMax = 0;
		double suffixMax = 0;
		double aux;
		int from = 0, to = 0, i;
		for(i = 0; i < args.length; i++)  {
			if((aux = Double.parseDouble(args[i])) +suffixMax > globalMax) {
				suffixMax += aux;
				globalMax = suffixMax;
				to = i;
			} else if(aux +suffixMax > 0) {
				if(suffixMax == 0) {
					from = i;
				}//si hubo un cambio en desde donde estamos considerando
				suffixMax += aux;
			} else {
				suffixMax = 0;
			}//vamos formando la máxima subsecuencia a partir de un sufijo
		}//recorremos toda la subsecuencia
		System.out.println("La m\u00E1xima subsecuencia consecutiva suma: " +globalMax);
		System.out.println("Y va de los \u00EDndices " + from + " al " + to + ", es decir:");
		for(i = from; i <= to; i++) {
			System.out.print(args[i] +", ");
		}//mostramos los índices que conforman la MCS
		System.out.println();
	}//main

}//MCS class

