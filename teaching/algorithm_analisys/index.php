<?
/**
 * Página con las notas del curso "Análisis de algoritmos".
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<html>
	<head>
		<title>Notas de clase - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas de análisis de algoritmos" />
		<meta name="keywords" content="notas, clase, datos, analisis, computacion, shell, ordenamiento, diseño, java, algoritmo, quicksort, mergesort" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de An&aacute;lisis de Algoritmos</h2>
						<p>El problema de ser positivo, es que Quicksort ser&iacute;a de tiempo lineal.</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Curso de quinto semestre de la licenciatura en Ciencias de la Computaci&oacute;n (plan 2013).
								El contenido del curso es sobre eficiencia computacional, centr&aacute;ndose en eficiencia temporal: como
								calcular la complejidad de un algoritmo, t&eacute;cnicas para eliminar ineficiencias, algoritmos de
								b&uacute;squeda, de ordenamiento, etc...</p>
							<p>As&iacute; con esta peque&ntilde;a introducci&oacute;n dada, va una lista de las notas que he elaborado para
								este curso:</p>
							<table><tbody><tr><td><h3><a href="4_sep - ejercicios de diseño.pdf" >Ejercicios de dise&ntilde;o de algoritmos</a></h3>
									Estas notas contienen una descripci&oacute;n detallada de algunos ejercicios que tienen la intenci&oacute;n
									de hacer practicar al lector el dise&ntilde;o de soluciones a problemas mediante algoritmos y abstracci&oacute;n
									matem&aacute;tica.</td></tr>
								<tr><td><h3><a href="9_sep - JavaT4P1.pdf" >Breve instructivo de programaci&oacute;n en Java</a></h3>
									Estas notas son un peque&ntilde;o instructivo de programaci&oacute;n en Java. De manera r&aacute;pida, se
									explica la sintaxis, operadores, objetos, tipos de datos...<br/>
									Las notas terminan con algunos ejercicios que tienen la intenci&oacute;n de utilizar los conocimientos
									adquiridos en Java para resolver problemas.</td></tr>
								<tr><td><h3><a href="11sep-Shellsortyalgoritmosdeordenamiento.pdf" >Shell sort y otros algoritmos de ordenamiento</a></h3>
									Explicaci&oacute;n detallada del ordenamiento de Shell, junto con otros algoritmos de ordenamiento</td></tr>
								<tr><td><h3><a href="max_con_seq/Spin-off Diseño de algoritmos_ Máxima subsecuencia consecutiva.pdf" >
									Dise&ntilde;o de la soluci&oacute;n al problema de la M&aacute;xima subsecuencia consecutiva</a></h3>
									En estas notas, se detalla una soluci&oacute;n al problema de la m&aacute;xima subsecuencia consecutiva.<br/>
									Las notas tienen el siguiente anexo: <a href="max_con_seq/MCS.java" >Soluci&oacute;n en Java</a>
								</td></tr>
							<!--tr><td><h3><a href="" ></a></h3></td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
