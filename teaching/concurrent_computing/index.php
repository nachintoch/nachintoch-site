<?
/**
 * Página que presenta los cursos dados.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<html>
	<head>
		<title>Notas de clase - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas de computo concurrente" />
		<meta name="keywords" content="notas, clase, algoritmos, computo, concurrente, computacion, distribuida, paralelo, java, python, c, c++" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de Computaci&oacute;n Concurrente</h2>
						<p>Sod hiols on sincronizaos rtartaorn ed escbirir seto</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Curso de octavo semestre de la licenciatura en Ciencias de la Computaci&oacute;n (plan 2013).
									El contenido es sobre los problemas te&oacute;ricos que se suponen que dos computadoras de alguna
									naturaleza, comparten memoria de alguna forma, para exhibir que esto puede resultar en problemas a
									ra&iacute;z de problemas no esperados. Se estudian t&eacute;cnicas, herramientas y estrategias mediante
									las cuales podemos dise&ntilde;ar nuestras soluciones a problemas de concurrencia.</p>
							<p>As&iacute; con esta peque&ntilde;a introducci&oacute;n dada, va una lista de las notas que he elaborado
							para este curso:</p>
							<table><tbody><tr><td><h3><a href="9ago-EjerciciosPeterson.pdf" >
									Ejercicios con el algoritmo de sincronizaci&oacute;n de Peterson</a></h3>
									Dando por hecho que el lector conoce el algoritmo de Peterson, las notas exponen ejercicios para mostrar
									el uso y utilidad del mismo.
									</td></tr>
								<tr><td><h3><a href="hilos_calendarizacion.pdf" >
									Hilos de ejecuci&oacute;n y calendarizaci&oacute;n de procesos</a></h3>
									Estas notas, exponen de forma detallada conceptos propios de sistemas operativos; que son fundamentales
									para el entendimiento de conceptos y eventos de la computaci&oacute;n concurrente.
									</td></tr>
								<tr><td><h3><a href="26-ago _ Excusión mutua y definiciones básicas.pdf" >Exclusi&oacute;n mutua</a></h3>
									Ya entrados en terreno, lo que exponen estas notas son m&aacute;s definiciones, pero ahora s&iacute;
									m&aacute;s propiamente en el terreno de la computaci&oacute;n concurrente.
									</td></tr>
								<tr><td><h3><a href="AlgoritmodeKessels" >Algoritmo de sincronizaci&oacute;n de Kessels</a></h3>
									En estas notas, se explica qu&eacute; es, c&oacute;mo funciona y en qu&eacute; casos podemos usar el algoritmo
									de sincronizaci&oacute;n de Kessels.<br/>
									Historico de versiones de estas notas: <a href="31ago-AlgoritmodeKessels" >Agosto de 2015 - Incluye una
									introducci&oacute;n al algoritmo del torneo</a>
								</td></tr>
								<tr><td><h3><a href="reposteria.pdf" >Algoritmo de la panader&iacute;a</a></h3>
									Sin salir del terreno de la sincronizaci&oacute;n, en estas notas expongo el algoritmo de la
									reposter&iacute;a o panader&iacute;a... Aunque siendo natal de Ciudad de M&eacute;xico, me gusta
									m&aacute;s llamarle "el algoritmo de la papeler&iacute;a del centro"... Ver&aacute;n porqu&eacute;.
								</td></tr>
								<tr><td><h3><a href="Primitivasdesincronizacion.pdf" >Primitivas de sincronizaci&oacute;n</a></h3>
									En estas notas, describo en una larga lista varias primitivas de sincronizaci&oacute;n, tambi&eacute;n
									llamadas "objetos". Pero ojo, este no es el mismo concepto de objeto que se usa en programaci&oacute;n
									orientada a objetos.
								</td></tr>
								<tr><td><h3><a href="7_oct - Aplicaciones de primitivas de sincronización.pdf" >
									Aplicaciciones de primitivas de sincronizaci&oacute;n</a></h3>
									En la misma l&iacute;nea que las notas anteriores, estas hablan sobre aplicaciones de primitivas de
									sincronizaci&oacute;n.
								</td></tr>
								<tr><td><h3><a href="Semaforos_monitores.pdf" >
									Colas concurrentes, sem&aacute;foros y moitores</a></h3>
									En estas notas, explico qu&eacute; son y c&oacute;mo se usan los sem&aacute;foros y monitores.
								</td></tr>
								<tr><td><h3><a href="21oct-Filosofoscomensales.pdf" >Problema de los fil&oacute;sofos comensales</a></h3>
									En estas notas, trato el problema de los fil&oacute;sofos comensales: qu&eacute; es y una "posible"
									forma de resolverlo.
								</td></tr>
								<tr><td><h3><a href="26 - 28_oct - Filósofos comensales_ apropiamiento y espera.pdf" >
									Soluci&oacute;n al problema de los fil&oacute;sofos comensales mediante apropiamiento y espera</a></h3>
									En la misma l&iacute;nea de las notas anteriores, se extiende el problema de los fil&oacute;sofos comensales,
									pero esta vez se propone una soluci&oacute;n real.
								</td></tr>
								<tr><td><h3><a href="28oct-Problemadelosfumadores.pdf" >Problema de los fumadores de tabaco</a></h3>
									En estas notas, se aborda y explica el problema de los fumadores.
								</td></tr>
								<tr><td><h3><a href="9Nov-Barberodormilon.pdf" >Problema del barbero dormil&oacute;n</a></h3>
									En estas notas, se aborda y explica el problema del barbero dormil&oacute;n.
								</td></tr>
								<tr><td><h3><a href="11nov-solucionesalproblemadelconsenso.pdf" >Soluciones al problema del consenso</a></h3>
									En estas notas, se da por hecho que el lector ya conoce el problema del consenso y se plantean varias
									formas de resolverlo.
								</td></tr>
								<tr><td><h3><a href="18nov-solucionesalconsensoconejemplos.pdf" >Ejemplos de soluciones al consenso</a></h3>
									Siguiendo la misma l&iacute;nea de las &uacute;ltimas notas, se presentan ejemplos de resoluciones al
									problema del consenso.
								</td></tr>
								<tr><td><h3><a href="23_nov - Ejemplos en Java y C solución al problema del consenso.pdf" >
									Ejemplos en Java y C soluci&oacute;n al problema del consenso</a></h3>
									En un &uacute;ltimo ejemplo, se muestran implementaciones en Java y C para resolver el problema del
									consenso.
									<br/>
									Las notas tienen el siguiente anexo:
									<a href="https://github.com/Nachintoch/ejemplos-concurrencia/tree/master" target="_blank"
										title="github" >C&oacute;digo de los ejemplos</a>
								</td></tr>
								<tr><td><h3><a href="intro_GUI.pdf" >Intoducci&oacute;n a las GUI</a></h3>
									Una aplicaci&oacute;n de la computaci&oacute;n concurrente, est&aacute; en el lado t&eacute;cnico del
									desarrollo de interfaces gr&aacute;ficas de usaurio. Cuando se trata de aplicaciones que reflejan
									cambios hechos por un solo usuario, como la mayor&iacute;a de los editores de texto o por un solo
									hilo/proceso, no hay mucho problema en cuanto a la concurrencia. Los verdaderos retos empiezan
									a aparecer cuando la aplciaci&oacute;n se hace de varios hilos trabajadores de segundo plano para
									tareas como obtener cada uno cierta informaci&oacute;n a partir de un servidor, realizar
									c&aacute;luclos intensivos etc.<br/>
									Esta aplicaci&oacute;n; como podr&aacute;n adivinar los clientes frecuentes, es una de mis favoritas,
									ya que nos conlleva casi inebitablemente a la programaci&oacute;n de videojuegos, donde tener que
									animar a personajes manipulados por la entrada de los jugdores, otros por la IA, otros por las reglas
									del juego; como las simulaciones f&iacute;sicas; etc. Involucran muchos hilos/procesos manipulando 
									el mismo videojuego: la concrrencia se pone divertida en m&aacute;s de un sentido.<br/>
									Las notas tienen el siguiente anexo: <a href="guis.zip" >Ejemplo de aplicaci&oacute;n g&aacute;ficas
									en Java Swing</a>
								</td></tr>
								<tr><td><h3><a href="18ago-Threads.pdf" >Hilos de ejecuci&oacute;n en Java</a></h3>
									Conforme nuestros proyectos y dise&ntilde;os crecen, es com&uacute;n que sea necesario trabajar a varios
									hilos. Un proceso (informalmente, un <i>programa en ejecuci&oacute;n</i>) puede ejecutar tantas
									instrucciones "simult&aacute;neas" como sea necesario. La capacidad de paralelizaci&oacute;n de un
									programa est&aacute; limitada &uacute;nicamente por la capacidad f&iacute;sica de la m&aacute;quina
									que ejecute el programa.<br/>
									Sin embargo, por muy maravilloso que esto suene, no es un panorama claro y hermoso del todo. Ya que es
									muy posible que el comportamiento de nuestro programa dependa de cuando se manipulan los varios hilos
									con los recursos compartidos. Es aqu&iacute; cuando vamos a empezar a lidiar con condiciones de carrera
									o competencia y tener que implementar estrategias de sincronizaci&oacute;n, como las que se mencionan
									en los puntos anteriores: implementar hilos es solo la primera parte.<br/>
									Las notas tienen el siguiente anexo: <a href="ejemplo_hilos.zip" >Ejemplo con hilos</a><br/>
									Tambi&eacute;n hacen referencia al ejemplo de calendarizaci&oacute;n mencionado en la secci&oacute;n de
									notas para <i>Estructuras de datos</i>.
								</td></tr>
								<tr><td><h3><a href="Colasconcurrentes.pdf" >Colas concurrentes</a></h3>
									Describe qu&eacute; son y c&oacute;mo implementar colas concurrentes.
								</td></tr>
								<tr><td><h3><a href="JavaConcurrent.pdf" >Herramientas de concurrencia en Java</a></h3>
									En estas notas, describo a grandes rasgos cu&aacute;les son y c&oacute;mo se usan las clases del
									paquete est&aacute;ndar de Java <tt>java.util.concurrent</tt>. Se pone &eacute;nfasis en los
									sem&aacute;foros.
								</td></tr>
								<tr><td><h3><a href="Concurrencia_sincronizacion_Python.pdf" >Herramientas de concurrencia y
									sincronizaci&oacute;n en Python</a></h3>
									De manera similar y an&aacute;loga a las notas sobre concurrencia con Java, se estudian las
									herramientas estandar que ofrece Python para resolver problemas de concurrencia. Se exploran
									las bibliotecas que nos permiten implementar las soluciones estudiadas en el curso en esta
									plataforma interpretada y favorita de muchos.<br/>
									Las  nota tienen el siguiente anexo: <a href="ejemplos_python.zip" >Ejemplos de sincronizaci&oacute;n
										en Python</a>
								</td></tr>
								<tr><td><h3><a href="Concurrencia_sincronizacion_C.pdf" >Herramientas de concurrencia y
									sincronizaci&oacute;n en C y C++</a></h3>
									An&aacute;logo al caso anterior, en estas notas se presentan las herramientas "est&aacute;ndares"
									(esto depende un poco m&aacute;s de la plataforma anfitriona y/o el compilador utilizado) para
									resolver problemas de concurrencia en C y C++; para tambi&eacute;n poder implementar soluciones
									a problemas de concurrencia con esta plataforma nativa.<br/>
									Las notas tienen el siguiente anexo: <a href="anexos_c.zip" >Ejemplos de
										sincronizaci&oacute;n en C y C++</a>
								</td></tr>
								<!--tr><td><h3><a href="" ></a></h3>
								</td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
