<?
/**
 * Página que presenta el curso de análisis de algiritmos.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, march 2016
 */
?>
<html>
	<head>
		<title>Estructuras de datos - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas para un curso superio de Introducci&oacute;n a las Estructruas de datos" />
		<meta name="keywords" content="estructuras, datos, xml, java, lista, arreglo, tda, tipo, abstracto, colas, pila, heap, grafica, ordenamiento, exploracion, iterador" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de Estructuras de Datos</h2>
						<p>The ADT Matrix has you</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Curso de segundo semestre de la licenciatura en Ciencias de la Computaci&oacute;n
							(plan 2013). El contenido es sobre estructuras de datos; como listas, pilas, colas, gr&aacute;ficas, etc...
							C&oacute;mo se define un Tipo de Datos Abstracto, qu&eacute; operaciones podemos definir sobre las estructuras y
							para que podemos usarlas.</p>
							<p>As&iacute; con esta peque&ntilde;a introducci&oacute;n dada, va una lista de las notas:</p>
							<table><tbody><tr><td><h3><a href="4feb-JavayXML.pdf" >Java y XML</a></h3>
									En estas notas, explico qu&eacute; es un XML y c&oacute;mo podemos usarlos en nuestra programaci&oacute;n
									con Java.<br/>
									Las notas tienen el siguiente anexo: <a href="ejemplos_XML.zip" >Ejemplos XML</a>.</td></tr>
								<tr><td><h3><a href="18feb-FileyXMLoutput.pdf" >Entrada y Salida con Java y m&aacute;s sobre Java y XML</a></h3>
									En estas notas, describo c&oacute;mo hacer lectura de archivos en Java, as&iacute; comos escribir archivos:
									tanto existentes, como crear archivos nuevos. Por otro lado, se extiende el contenido sobre XML para
									incluir c&oacute;mo escribir nuestros propios XML (o modificar un archivo ya existente).<br/>
									Las notas tienen el siguiente anexo: <a href="ejemplosIO.zip" >Ejemplos entrada y salida</a></td></tr>
								<tr><td><h3><a href="25_feb-Benchmarks y recursión.pdf" >Recursi&oacute;n y Benchmarks</a></h3>
									En estas notas, describo qu&eacute; es la recursi&oacute;n. Entro en aspectos un poco m&aacute;s t&eacute;cnicos
									que en formalidades matem&aacute;ticas. Tambi&eacute;n se explica que es un benchmark y para qu&eacute;
									se utilizan.<br/>
									Las notas tienen el siguiente anexo: <a href="ejemplo_recursion.zip" >Ejemplo de recursi&oacute;n</a>.</td></tr>
								<tr><td><h3><a href="1mar-Backtracking.pdf" >Backtracking</a></h3>
									En estas notas, describo qu&eacute; es el backtracking, usando los conocimientos previos en recursi&oacute;n
									de las notas anteriores. Se ejemplifica el backtracking con el problema de resolver un laberinto.<br/>
									Las notas tienen el siguiente anexo: <a href="ejercicio_backtracking.zip" >Ejercicio de backtracking</a>.
								</td></tr>
								<tr><td><h3><a href="8mar-Listas.pdf" >Ejemplos de Listas</a></h3>
									En estas notas, se introduce el <b>Tipo de Datos Abstracto Lista</b> y se propone un par de ejercicios:
									<ul>
										<li>Reconocimiento de palabras (o <i>"toknes"</i>) dadas por el usuario. Estas palabras deber&aacute;n
											relacionarse con todas las l&iacute;neas en las que aparecen en un archivo de texto que proporcione
											tambi&eacute;n el usaurio.</li>
										<li>El <i>problema de Jos&eacute;</i>, como se le conoce a la adaptaci&oacute;n presentada a un problema
											planteado originalmente por el historiador Flavius Josephus en el primer siglo de nuestra era.</li>
									</ul>
									Las notas tienen el siguente anexo: <a href="ejemplos_listas.zip" >Ejercicios con listas</a>
								</td></tr>
								<tr><td><h3><a href="29_mar-programación generica.pdf" >Programaci&oacute;n gen&eacute;rica</a></h3>
									En estas notas, describo qu&eacute; es la programaci&oacute;n gen&eacute;rica en Java y c&oacute;mo hacer
									una clase gen&eacute;rica, siguiendo como ejemplo las clases <tt>java.util.ArrayList</tt>,
									<tt>java.util.LinkedList</tt> y <tt>java.util.Vector</tt>
								</td></tr>
								<tr><td><h3><a href="5abr-TorresdeHanoi.pdf" >Soluci&oacute;n al problema de las torres de Hanoi</a></h3>
									En estas notas, se plantea una soluci&oacute;n al problema de las Torres de Hanoi y se plasma con un
									algoritmo recursivo. Luego, se da el reto de en base a dicha soluci&oacute;n recursiva, construir una
									iterativa usando una sola pila, que deber&aacute; contener una representaci&oacute;n de los contextos
									de cada paso recursivo.<br/>
									Las notas tienen el siguiente anexo: <a href="solucion_hanoi.zip" >Soluci&oacute;n en Java</a>.
								</td></tr>
								<tr><td><h3><a href="7abr-Colasprocesosyround-robin.pdf" >Ejemplo de colas con planificaci&oacute;n
									<i>Round - Robin</i>: Calendarizaci&oacute;n de procesos</a></h3>
									Estas notas tratan un tema un tanto complicado para los primeros semestres de Ciencias de la
									Computaci&oacute;n o Ingenir&iacute;a en computaci&oacute;n. Nos introduce a una de las t&eacute;cnicas
									m&aacute;s populares de calendarizaci&oacute;n de procesos (aka. "Cosas que tiene que hacer un procesador")
									para ejemplificar la utilidad y uso de la estructura de datos "Cola" mediante planificaci&oacute;n
									Round - Robin.<br/>
									Las notas tratan de fomentar la curiosidad del lector y puede consultar las referencias usadas para
									su elaboraci&oacute;n para obtener m&aacute;s informaci&oacute;n al respecto de la calendarizaci&oacute;n
									de procesos en un sistema operativo. Si encuentra dificultades al entender los conceptos de Sistemas
									Operativos y Concurrencia que introducen estas notas, puede ignorarlos y concentrarse en el uso y
									funcionamiento de la cola que simula el ejercicio.<br/>
									Las notas tienen los siguientes anexos: <ul><li><a href="ejemplo_calendarizador.zip" >Ejemplo desarrollado
										de calendarizaci&oacute;n de procesos mediante Round - Robin</a></li>
										<li><a href="Conceptos en el ejemplo de calendarización de procesos.pdf" >Notas adicionales con los
										conceptos introducidos en el ejemplo de calendarizaci&oacute;n de procesos</a></li></ul>
								</td></tr>
								<tr><td><h3><a href="12abr-Ejemploscolas.pdf" >M&aacute;s ejemplo de colas</a></h3>
									Usando ejemplos que se espera sean m&aacute;s simples que el de calendarizaci&oacute;n de procesos,
									se ejemplifica la utilidad y uso de colas con los siguientes problemas:
									<ul>
										<li>El <i>problema de acomodo de trenes</i>. El problema consiste b&aacute;sicamente en acomodar
											una serie de vagones de tren etiquetados con n&uacute;meros naturales un&iacute;vocos de
											forma que las etiqeutas en el tren acomodado sean de orden creciente.</li>
										<li>Migrar la soluci&oacute;n al problema de Jos&eacute; para ahora usar una cola en lugar de
											una lista.</li>
									</ul>
									Las notas tienen el siguiente anexo: <a href="ejercicios_colas.zip" >Ejercicios con colas</a>
								</td></tr>
								<tr><td><h3><a href="21_abr-EjemploarbolesSA.pdf" >Aplicaciones de &aacute;rboles</a></h3>
									En estas notas revisamos una aplicaci&oacute;n de la estructura de datos &aacute;rbol. Los &aacute;rboles
									son una de las estructuras de datos m&aacute;s &uacute;tiles en computaci&oacute;n; nos ayudan a organizar
									conjunto de datos, agrupados por alguna caracter&iacute;stica com&uacute;n en sub&aacute;rbobles. Por
									ejemplo, una desigualdad tricot&oacute;mica, que puede resultar en un orden total o parcal; respectivamente
									cuando permitimos valores repetidos o &uacute;nivocos en cada nodo del &aacute;rbol. Esto hace las
									b&uacute;squedas tremendamente eficientes.<br/>
									Tambi&eacute;n podemos usarlos para representar lenguajes formales y crear compiladores de lenguajes de
									programaci&oacute;n, modelar colas de prioridades y formar procesos en un &aacuter;bol, como hacen algunos
									sistemas operativos.<br/>
									Y el caso que vamos a estudiar: modelas sistemas de archivos.<br/>
									Las notas tienen el siguiente anexo: <a href="ejemplo_arboles_archivo.zip" >Ejemplo de explorador de
									archivos</a>
								</td></tr>
								<tr><td><h3><a href="Codigos_hash.pdf" >C&oacute;digos Hash</a></h3>
									En esta lectura, se estudian y exploran las funciones hash: qu&eacute; es una tabla hash (o diccionario/mapa
									parcialmente ordenado), qu&eacute; es una funci&oacute;n de compresi&oacute;n, qu&eacute; es una
									colisi&oacute;n y una breve introducci&oacute;n a c&oacute;mo usar hashing en Java.
								</td></tr>
								<!--tr><td><h3><a href="" ></a></h3></td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>