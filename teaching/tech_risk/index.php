<?
/**
 * Página que presenta los cursos dados.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<html>
	<head>
		<title>Notas de clase - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas de riesgo tecnologico" />
		<meta name="keywords" content="notas, clase, riesgo, tecnologico, proyecto, desarrollo, java, ruby, python, rails, django, google, maps, javascript, jquery" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de clase de Riesgo Tecnol&oacute;gico</h2>
						<p>Tu cliente quiere m&aacute;s cambios que olvidaste anexar al contrato</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Curso optativo de la licenciatura en Ciencias de la Computaci&oacute;n (plan 2013).
								El contenido es sobre el an&aacute;lisis de los riesgos que pueden presentarse al desarrollar un
								producto de software o una investigaci&oacute;n, ya sea por medio de auto-financiamientos,
								presupuestos, programas de investigaci&oacute;n...</p>
							<p>As&iacute; con esta peque&ntilde;a introducci&oacute;n dada, va una lista de las notas que he
							elaborado para este curso:</p>
							<table><tbody><tr><td><h3><a href="15feb-JavaEE.pdf" >Introducci&oacute;n a Java Enterprise Edition</a></h3>
									En estas notas, se explica qu&eacute; es Java Enterprise Edition y se desarrolla un
									proyecto de ejemplo.</td></tr>
								<tr><td><h3><a href="24-febdibjar_java_web_DB.pdf" >
									C&oacute;mo crear una imagen PNG con Java y c&oacute;mo usar el Canvas de HTML5</a></h3>
									Siguiendo la misma l&iacute;nea de las notas anteriores, se explica c&oacute;mo crear gr&aacute;ficos
									con Java Enterprise Edition de distintas formas. Al final, tambi&eacute;n se explica como enlazar
									nuestra aplicaci&oacute;n con una dase de datos PostgreSQL.<br/>
									Las notas tienen los siguientes anexos:<ul><li><a href="EjemploJavaEE.zip" >Proyecto de Ejemplo Java EE</a></li>
									<li><a href="ejemplo_canvas" target="_blank" >Ejemplo de Canvas HTML5 (vea el c&oacute;digo fuente de la
									p&aacute;gina)</a></li></ul></td></tr>
								<tr><td><h3><a href="2mar-IntroaRuby.pdf" >Introducci&oacute;n a Ruby on Rails</a></h3>
									En estas notas, se introduce al lector brevemente al lengaje de programaci&oacute;n Ruby y qu&eacute; es
									el framework Ruby on Rails y se desarrolla una aplicaci&oacute;n de ejemplo</td></tr>
								<tr><td><h3><a href="16mar-SeguridadyRails.pdf" >Seguridad web con Rails</a></h3>
									Siguiendo la l&iacute;nea de las notas pasadas, para introducir conceptos y t&eacute;cnicas de seguridad
									web para construir un manejo de sesiones de usuario seguras. Todo esto ampliando el ejemplo de las
									&uacute;ltimas notas para inclusi&oacute;n de seguridad.<br/>
									Las notas tienen el siguiente anexo: <a href="ejemploRubyRails.zip" >Ejemplo de Ruby on Rails y seguridad web</a>
								</td></tr>
								<tr><td><h3><a href="IntroaPythonDjango.pdf" >Introducci&oacute;n a Django</a></h3>
									En estas notas, se introduce al lector brevemente al lenguaje de programaci&oacute;n Python y qu&eacute;
									es el framework Django y se desarrolla una aplicaci&oacute;n de ejemplo.
								</td></tr>
								<tr><td><h3><a href="6_abr - Manipulación de gráficos.pdf" >Manipuaci&oacute;n de gr&aacute;ficos con Python y
									API de Google Maps (Python:Django + Javascript:JQuery)</a></h3>
									Siguiendo la misma l&iacute;nea  de las notas pasadas, inducimos el uso de JQuery y Javascript con
									nuestros proyectos en Django (Python) para mostrar una p&aacute;gina con un mapa de Google en donde
									se introduce al lectro al uso de APIs externas con Javascript para p&aacute;ginas din&aacute;micas,
									en particulas: el API de Google Maps.<br/>
									Las notas tiene el siguiente anexo: <a href="ejemploDjango.zip" >Ejemplo de Django con API de Google Maps</a>
								</td></tr>
								<!--tr><td><h3><a href="" ></a></h3></td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
