<?
/**
 * Página con las notas del curso de "Desarrollo de Dispositivos Móviles"
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, april 2019
 * @since Nachintoch.mx 1.0, april 2019
 */
?>
<html>
	<head>
		<title>Notas de clase - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas de programacion de dispositivos moviles" />
		<meta name="keywords" content="notas, clase, movil, java, tablet, celular, smartphone, android, sdk, jetpack, smartwatch, wear os, ingenieria, software,desarrollo" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de clase de Programación de Dispositivos M&oacute;viles</h2>
						<p>Desafortunadamente, la aplicación Duelo de Vaqueros se ha detenido</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Curso optativo de la licenciatura en Ciencias de la Computaci&oacute;n (plan 2013).
								El contenido del curso es sobre
								<span style="font-weight:bold">desarrollo de Android.
								</span> Comienza con consideraciones m&aacute;s
								b&aacute;sicas sobre desarrollo m&oacute;vil en general,
								luego introduce al <span style="font-style:italic">
								Android SDK</span> y <span style="font-style:italic">
								Android Studio</span> para empezar a explorar la plataforma
								de Android y las herramientas y conceptos
								m&aacute;s b&aacute;sicos para empezar a desarrollar aplicaciones con Android.</p>
							<p>Particip&eacute; en la impartici&oacute;n de este
							curso durante la pandemia por COVID-19; que motiv&oacute;
							a trabajar los cursos a distancia. Hace algunos a&ntilde;os,
							me preguntaba porqu&eacute; la Facultad de Ciencas no ofrece
							ni una carrera en Modalidad Abierta o a Distancia. La
							respuesta que recib&iacute; por varios colegas en ese momento
							fue que "las carreras de ciencias son muy dif&iacute;ciles"
							(y cu&aacute;l no) "y que por eso no se pueden dar a
							distancia"... Hasta que lo intentaron y resulta que siempre
							s&iacute; se pod&iacute;a, solo hac&iacute;a falta intentarlo.
							</p>
							<p>Las clases a distancia <span style="font-weight:bold">
							necesitan</span> un modelo MUY diferente al de las clases
							presenciales; y si existe una opini&oacute;n generalizada sobre
							la "mala" calidad de la ense&ntilde;anza a distancia, tiene
							mucho que ver con que muchos docentes no fueron capacitados
							-o simplemente se negaron a cambiar su forma de trabajo- y
							durante la pandemia se present&oacute; un modelo de
							educaci&oacute;n "presencial a distnacia" que simplemente no
							dio el ancho. No solo lo v&iacute; como docente, sino
							tambi&eacute;n como estudiante de posgrado.</p>
							<p>Tratando de adecuar este curso para la modalidad en
							l&iacute;nea, hice una reestructuraci&oacute;n significativa
							a la organizaci&oacute;n de los temas y grab&eacute;
							las clases. Ahora presento ese material y las video clases
							en esta secci&oacute;n.</p>
							<p>Los videos se encuentran hospedados en la comunidad
							<a href="https://video.hardlimit.com/" target="_blank">
							HardLimit</a>, que re&uacute;ne entusiastas de la
							tecolog&iacute;a digital y cuenta con una plataforma de video
							basada en <a href="https://joinpeertube.org/" target="_blank" >
							PeerTube</a>.<br/>
							Recomiendo que abra el enlace dando click en el texto "PeerTube"
							en la esquina inferior derecha del (los) video(s) que desee
							consultar, pues en HardLimit los videos cuentan con una
							<span style="font-weight:bold">descripci&oacute;n con marcas
							de tiempo que desglozan detalladamente el contenido de cada
							video</span>.</p>
							<p>Para su f&aacute;cil acceso, dejo el
							<a href="https://video.hardlimit.com/w/p/qDAdth9SwvtyFV2tZq7enH" target="_blank" >
							enlace a la lista de reproducc&oacute;n</a> en HardLimit.</p>
							<p>Tras esta ya no tan peque&ntilde;a
						 	introducci&oacute;n, va una lista de los temas que he
							elaborado para este curso:</p>
							<table><tbody><tr><td><h3><a href="introduccion.pdf" >Introducci&oacute;n a Android</a></h3>
								<iframe title="1 - Introducción a Android"
								src="https://video.hardlimit.com/videos/embed/7ac1bd09-b026-4acb-acb5-f5e418d2b301"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe><br/>
								Empezamos el curso con una introducci&oacute;n muy general
								al desarrollo m&oacute;vil, la historia de
								Android y sus caracter&iacute;sticas como Plataforma de
								desarrollo y Sistema Operativo</td></tr>
							  <tr><td><h3><a href="consideraciones-generales-dev-mob.pdf" >Consideraciones generales
								  del desarrollo m&oacute;vil</a></h3>
								<iframe
								title="2 - Consideraciones Generales del Desarrollo para Móviles"
								src="https://video.hardlimit.com/videos/embed/5d3912d9-e251-4ca5-9f77-826686b6287e"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								Antes de empezar a conocer las herramientas con las que
								vamos a desarrollar aplicaciones para Android, vamos a
								revisar algunas de las consideraciones del desarrollo
								m&oacute;vil m&aacute;s importantes.</td></tr>
							  <tr><td><h3><a href="Android-Studio.pdf" >Android Studio
							  </a></h3>
							    <iframe title="3 - Android Studio"
								src="https://video.hardlimit.com/videos/embed/7ddc3509-f113-48fc-afcd-caef859f559e"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								Empecemos a conocer la herramienta principal con la que desarrollamos aplicaciones Android:
								  <span style="font-style:italic">Android Studio</span>
							  </td></tr>
							  <tr><td><h3><a href="trabajo-en-equipo.pdf" >Trabajo en
								  equipo</a></h3>
								<h4>Tema opcional</h4>
								<iframe
								title="4 Parte 1 - Desarrollo de aplicaciones en Equipo"
								src="https://video.hardlimit.com/videos/embed/55e3f20b-e82c-41a9-ba0b-9c961955173b"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								<p>La estrategia de trabajo del curso; en el que al final
								los estudiantes completan una aplicaci&oacute;n para un
								cliente real, sumada al trabajo a distancia por la
								pandemia; hacen que sea necesario revisar algunos conceptos
								en torno a la <span style="font-style:italic">
								Ingenier&iacute;a de Software</span>.</p>
								<p>En este tema se introducen conceptos y pr&aacute;ctias
								para el desarrollo en equipo; basadas en
								Metodolog&iacute;as &Aacute;giles, Dise&ntilde;o Centrado
								en el Usuario y Colaboraci&oacute;n as&iacute;ncrona no
								precencial.</p>
								<p>Porque es posible crear software de calidad sin tener
								que compartir el mismo espacio que tus compa&ntilde;eros
								de trabajo (aunque compartir una cerveza durante una
								reta de Mario Kart siempre es deseable).</p>
							  </td></tr>
							  <tr><td><h3><a href="dev-apps.pdf" >Desarrollando
								  aplicaciones para Android</a></h3>
								<iframe
								title="4 Parte 2 - Desarrollo de la aplicación &quot;Duelo de Vaqueros&quot;"
								src="https://video.hardlimit.com/videos/embed/ef1c8b51-5aff-4786-a7e8-c6ccb68dd8e6" allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe><br/>
  								<p>En estas notas se presenta un proyecto que muestra
								algunos de los conceptos más básicos que son necesarios
								para empezar a desarrollar aplicaciones para Android.</p>
  								<p>El proyecto que se desarrolla en estas notas est&aacute; disponible en Gitlab: <a target="_blank"
								href="https://gitlab.com/nachintoch/cowboy-duel" >
  								https://gitlab.com/nachintoch/cowboy-duel</a></p>
								<p>Por &uacute;ltimo, para desarrollar el proyecto se
								cuentan con los siguientes recursos (im&aacute;genes y
								sonidos para replicar la aplicaci&oacute;n de ejemplo,
								pero puede usar otros si lo desea): <a
								href="recursos.zip" >recursos.zip</a></p>
  							  </td></tr>
							  <tr><td><h3><a href="material-design.pdf" >Material design
							  	  </a></h3>
								<iframe title="5 - Interfaces Gráficas con Material Design"
								src="https://video.hardlimit.com/videos/embed/af18e522-375d-483a-bb9e-b41111956078"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								<p>El lenguaje de diese&ntilde;o est&aacute;ndar de Android
								es <span style="font-style:italic">Material Design
								</span>. Estas notas introducen algunos de los fundamentos
								m&aacute;s b&aacute;sicos para implementar interfaces
								gr&aacute;ficas en Android siguiendo el estandar de
								dise&ntilde;o.</p>
								<p>Este tema est&aacute; dividido en dos notas, la
								<a href="intro-guis.pdf" >primera parte puede ser accedida aqu&iacute;</a> (el video incluye ambas partes).</p>
								<p>Estas notas inician el desarrollo de un proyecto que se retoma en varias notas m&aacute;s adelante.
								<a target="_blank"
								href="https://gitlab.com/nachintoch/gui-design-patterns-w-material-desig" >
								https://gitlab.com/nachintoch/gui-design-patterns-w-material-desig</a></p>
							  </td></tr>
							  <tr><td><h3><a href="ciclo-de-vida.pdf" >Ciclo de vida de una
								  aplicaci&oacute;n Android</a></h3>
								<iframe title="6 - Ciclo de Vida de una Aplicación Android"
								src="https://video.hardlimit.com/videos/embed/666b01c5-91ae-4882-b7b7-1fbbf75ec4bc"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								<p>Todas las aplicaciones, programas y sistemas tienen un
								ciclo de vida: son instanciadas, puestas en marcha,
								consumen recursos, son pausadas y eventualmente crashean
								(o su ejecuci&oacute;n termina).</p>
								<p>En este tema conoceremos c&oacute;mo es el ciclo de vida
								de una aplicaci&oacute;n Android, y el marco de trabajo
								que delimitan para nuestras apps.</p>
								<p>Este tema est&aacute; dividido en dos notas, la
								<a href="documentacion-proyectos.pdf" >segunda parte</a>
								nos sugiere como dise&ntilde;ar y sobre todo documentar
								una aplicaci&oacute;n Android, usando el marco de trabajo
								que delimita su Ciclo de Vida (el video incluye ambas
								partes).</p>
							  </td></tr>
							  <tr><td><h3><a href="Fragmentos.pdf" >Fragmentos</a></h3>
								<iframe title="7 - Fragmentos"
								src="https://video.hardlimit.com/videos/embed/bf3a5288-f889-4b64-b647-655bc8f43cf6"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								<p>En estas notas, se retoma el proyecto de ejemplo de Material Design y se continua su desarrollo para conocer
								los fragmentos, que son pantallas que pueden acomodarse de
								la forma en que mejor nos convenga para implementar las
								interfaces de nuestras apps. Adem&aacute;s, a diferencia
								de un Layout, los Fragmentos se definen en c&oacute;digo;
								por lo que son pantallas con atributos y comportamiento.
								Los fragmentos se usan para implementar uno de los patrones
								de dise&ntilde;o de Android m&aacute;s preciados:
								<span style="font-style:italic">Master-Detail Flow</span>.
							    </p>
								<p>Los fragmentos tambi&eacute;n se usan para implementar
								pantallas "predefinidas" que podemos usar para desarrollar
								r&aacute;pido. Se agregar&aacute; una Pantalla de
								Preferencias a la aplicaci&oacute;n que ejemplifica
								Material Design tambi&eacute;n</p>
								<p>Enlace al repositorio de ejemplo: <a target="_blank"
								href="https://gitlab.com/nachintoch/gui-design-patterns-w-material-desig" >
								https://gitlab.com/nachintoch/gui-design-patterns-w-material-desig</a></p>
							  </td></tr>
							  <tr><td><h3><a href="contentproviders.pdf" >Proveedores de
									Contenidos</a></h3>
							    <iframe title="8 - Proveedores de contenidos"
								src="https://video.hardlimit.com/videos/embed/d4708312-0c5e-4e9e-90c2-dd26eb2b8d86"
								allowfullscreen=""
								sandbox="allow-same-origin allow-scripts allow-popups"
								width="560" height="315" frameborder="0"></iframe><br/>
								<p>En este tema conoceremos el componente de una
								aplicaci&oacute;n Android que nos permite definir
								repositorios de informaci&oacute;n que podemos incluso
								compartir con otras aplicaciones (si as&iacute; lo
								configuramos).<br/>
								El almacenamiento es independiente de la
								implementaci&oacute;n por lo que aunque el proyecto
								se ejemplifica usando la implementaci&oacute;n nativa
								de SQLite de Android, pero podr&iacute;amos usar otros
								manejadores de bases de datos, almacenamiento en la
								nube, archivos locales o en otros medios.</p>
								<p>Enlace al proyecto de ejemplo: <a target="_blank"
								href="https://gitlab.com/nachintoch/gui-design-patterns-w-material-desig" >
								https://gitlab.com/nachintoch/gui-design-patterns-w-material-desig</a>
							  </td></tr>
							  <tr><td><h3>Diálogos emergentes con FragmentDialog</h3>
								  <iframe title="Android - Dialogos Personalizados con DialogFragment y AlertDialog.Builder" src="https://video.hardlimit.com/videos/embed/b6ee3961-9349-480f-b876-e3c718bab9ba"
								    allowfullscreen=""
									sandbox="allow-same-origin allow-scripts allow-popups"
									width="560" height="315" frameborder="0">
								</iframe>
								<p>Las actividades y fragmentos de una
									aplicaci&oacute;n Android pueden configurarse
									para ser mostrados en forma de cuadros de
									texto o dialogos emergentes. Una de las
									estrategias m&aacute;s empleadas para construir
									Dialogos emergentes es definiendo subclases
									de <span style="font-weight:bold" >
										DialogFragment<span></p>
								<p>Encontrar&aacute; el repositorio del proyecto
									de ejemplo que se construye en el video en
									la siguiente liga:
									<a target="_blank"
										href="https://gitlab.com/nachintoch/ejemplo-dialogo-personalizado" >
										https://gitlab.com/nachintoch/ejemplo-dialogo-personalizado
									</a></p>
							  </td></tr>
							  <tr><td><h3><a href="dev-apps-ar.pdf" >
								  Realidad Aumentada (RA) con SceneForm
								  </a></h3>
								  <p>Pese a que Google ha sido inconsistente
								  	respecto a la presencia de Realidad Aumentada
									y Virtual en Android, la comunidad ha mantenido
									paquetes que permiten una f&aacute;cil
									integraci&oacute;n  de estas tecnologías en
									nuestras aplicaciones... Con ciertas
									limitaciones...<br>
									En estas notas se presenta c&oacute;mo usar
									<span style="font-style:italic" >SceneForm
									</span> para crear una simple aplicación que
									aumenta rostros empleando la c&aacute;mara
									frontal del dispositivo, pudiendo aumentar
									los rostros mediante texturas (que pueden
									implementarse cómo imágenes PNG que SceneForm
									dibuja sobre el contorno de los rostros
									detectados) o mediante modelos 3D</p>
								  <p>El proyecto que se desarrolla en estas notas se
								  encuentra hospedado en el siguiente repositorio:
								  <a target="_blank"
								  	href="https://gitlab.com/nachintoch/ejemplo-ar-sceneview" >
									  https://gitlab.com/nachintoch/ejemplo-ar-sceneview
								  </a></p>
							  </td></tr>
							  <!--tr><td><h3><a href="" ></a></h3></td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
