<?
/**
 * Página con las notas del curso "Arquitectura de Software".
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, september 2024
 * @since Nachintoch.mx 1.0, september 2024
 */
?>
<html>
	<head>
		<title>Notas de clase - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas de arquitectura de software" />
		<meta name="keywords" content="notas, clase, arquitectura, software, stakeholder, interesado, requerimiento, diseño, modelo, decision, proceso, calidad" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de Arquitectura de software</h2>
						<p>Tu diseño es tan fragil que al mover de lugar un
                            bot&oacute;n en una interfaz, los hilos de segundo
                            plano se detienen por referencias nulas.</p>
					</header>
					<div class="row 150%">
						<section id="content">
                            <p>Curso de octavo semestre de la licenciatura en
                            Ingenier&iacute;a de Software (UACM plan 2010). El
                            curso gira en torno a actividades de dise&ntilde;o
                            general de un producto de software: la
                            integraci&oacute;n de sus componentes y
                            definici&oacute;n de puntos de flexibilidad que
                            permitan al producto alcanzar sus objetivos,
                            satisfaciendo a todos los interesados relevantes. Se
                            exploran estrategias de dise&ntilde;o
                            arquitectonico, modelaje de arquitecturas y el rol
                            de un arquitecto de software en un proyecto.</p>
                            <table><tbody>
                                <tr><td><h3><a href="1_3-componentes.pdf" >
                                    Definiciones b&aacute;sicas, componentes y
                                    conectores</a></h3>
                                Estas notas incluyen algunas definiciones
                                generales empleadas en el &aacute;rea de la
                                arquitectura de software, as&iacute; como una
                                breve presentaci&oacute;n de los conceptos de
                                "componentes" y "conectores", su importancia e
                                impacto general en la calidad de un producto de
                                software.
                                </td></tr>
                            </tbody></table>
                        </section>
                    </div>
                </div>
            </div>
        <!-- Footer -->
        <? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
    </body>
</html>
