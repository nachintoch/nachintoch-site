<?
/**
 * Página que presenta los cursos dados.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Notas de clase - Nachintoch Desarrollos</title>
		<meta name="description" content="Notas de los cursos en los que he participado - conocimiento al alcance de todos" />
		<meta name="keywords" content="notas, clase, estructuras, datos, analisis, algoritmos, computo, concurrente, computacion, distribuida, paralelo, riesgo, tecnologico, proyecto, desarrollo" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Notas de clase</h2>
						<p>Conocimiento al alcance de todos</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Durante la segunda mitad del a&ntilde;o 2015, se me present&oacute; por primera vez la oportunidad
							de laborar como docente en forma de ayudante de
							profesor en la Facultad de Ciencias de la
							Universidad Nacional Aut&oacute;noma de
							M&eacute;xico; misma donde hice la licenciatura y
							gracias a la invitaci&oacute;n de uno de mis
							m&aacute;s estimados profesores.</p>
							<p>Pararse frente al pizarr&oacute;n para exponer
								temas a otras personas me ha resultado
								fascinante. En estos a&ntilde;os he confirmado
								que la docencia es una de mis ocupaciones
								favoritas.</p>
							<p>Para las clases que imparto me gusta tener notas
								preparadas que quiero compartir con
							todos en &eacute;sta p&aacute;gina (con permiso del
							profesor titular en caso de que no sea yo).</p>
							<p>As&iacute; con esta peque&ntilde;a introducci&oacute;n dada, va una lista de los cursos (t&oacute;picos)
							de los que dispongo notas:</p>
							<table><tbody><tr><td><h3><a href="intro_data_structure" >Estructuras de datos</a></h3>
									Curso de segundo semestre de la licenciatura en Ciencias de la Computaci&oacute;n
									(UNAM plan 2013). El contenido es sobre estructuras de datos; como listas, pilas, colas, gr&aacute;ficas, etc...
									C&oacute;mo se define un Tipo de Datos Abstracto, qu&eacute; operaciones podemos definir sobre las estructuras y
									para que podemos usarlas.</td></tr>
								<tr><td><h3><a href="algorithm_analisys" >An&aacute;lisis de Algoritmos</a></h3>
									Curso de quinto semestre de la licenciatura en Ciencias de la Computaci&oacute;n (UNAM plan 2013).
									El contenido del curso es sobre eficiencia computacional, centr&aacute;ndose en eficiencia temporal: como
									calcular la complejidad de un algoritmo, t&eacute;cnicas para eliminar ineficiencias, algoritmos de
									b&uacute;squeda, de ordenamiento, etc...</td></tr>
								<tr><td><h3><a href="concurrent_computing" >C&oacute;mputo concurrente</a></h3>
									Curso de octavo semestre de la licenciatura
									en Ciencias de la Computaci&oacute;n (UNAM
									plan 2013). El contenido es sobre problemas
									te&oacute;ricos que suponen dos computadoras
									que comparten memoria de alguna forma,
									exhibiendo que esto puede resultar en
									problemas a ra&iacute;z de comportamientos
									no esperados. Se estudian t&eacute;cnicas,
									herramientas y estrategias mediante las
									cuales podemos dise&ntilde;ar soluciones a
									problemas de concurrencia.</td></tr>
								<tr><td><h3><a href="sw_architecture" >
									Arquitectura de software</a></h3>
									Curso de octavo semestre de la licenciatura
									en Ingenier&iacute;a de Software (UACM plan
									2010). El curso gira en torno a actividades
									de dise&ntilde;o general de un producto de
									software: la integraci&oacute;n de sus
									componentes y definici&oacute;n de puntos
									de flexibilidad que permitan al producto
									alcanzar sus objetivos, satisfaciendo a
									todos los interesados relevantes. Se
									exploran estrategias de dise&ntilde;o
									arquitectonico, modelaje de arquitecturas y
									el rol de un arquitecto de software en un
									proyecto.
								</td></tr>
							 <tr><td><h3><a href="mobile_dev" >
                                 Programaci&oacute;n de dispositivos
                                 m&oacute;viles</a></h3>
							    <p>Curso optativo de la licenciatura en Ciencias
                                de la Computaci&oacute;n (UNAM plan 2013).
								  El contenido es sobre <span
                                  style="font-weight:bold">desarrollo de
                                  Android.</span> Comienza con consideraciones
                                  m&aacute;s b&aacute;sicas sobre desarrollo
                                  m&oacute;vil en general, luego introduce al
                                  <span style="font-style:italic">Android SDK
                                  </span> y <span style="font-style:italic">
								  Android Studio</span> para empezar a explorar
                                  la plataforma de Android y las herramientas y
                                  conceptos m&aacute;s b&aacute;sicos para
                                  empezar a desarrollar aplicaciones con
                                  Android.</p>
                                  <p>Debido a la pandemia de COVID-19, la
                                  modalidad de este curso se volvi&oacute;
                                  virtual; por lo que grab&eacute; la
                                  mayor&iacute;a de los temas. Las <span
                                  style="font-weight:bold">Video Clases</span>
                                  y las pr&aacute;cticas del curso se encuentran
                                  disponibles en esta secci&oacute;n</p>
                                </td></tr>
								<tr><td><h3><a href="tech_risk" >Riesgo
                                  Tecnol&oacute;gico</a></h3>
                                  Curso optativo de la licenciatura en Ciencias
                                  de la Computaci&oacute;n (UNAM plan 2013). El
                                  contenido es sobre el an&aacute;lisis de los
                                  riesgos que pueden presentarse al desarrollar
                                  un producto de software o una
                                  investigaci&oacute;n, ya sea por medio de
                                  auto-financiamientos, presupuestos, programas
                                  de investigaci&oacute;n...
								</td></tr>
							<tr><td><h3><a href="notas_CIDW.pdf" >
								Introducci&oacute;n al desarrollo Web con NodeJS
								</a></h3>
								Curso extracurricular dirigido a estudiantes de
								&uacute;ltimos semestres o egresantes de
								carreras afines a las Ciencias de la
								Computaci&oacute;n, Ingenier&iacute;a en
								Computaci&oacute;n e Inform&aacute;tica.
								El curso introduce el desarrollo Web comenzando
								por aspectos muy generales, como el protocolo
								HTTP y la arquitectura Modelo-Vista-Controlador,
								pasando por JavaScript, sus
								caracter&iacute;sticas y estrategias de uso
								recomendadas, aplicando al final el contenido
								el el desarrollo de una aplicaci&oacute;n Web
								con NodeJS que gestiona Altas, Bajas y Consultas
								de usuarios, incluye la implementaci&oacute;n
								de interfaces mediante plantillas de vistas HTML
								y su presentaci&oacute;n con jQuery y Bootstrap.
							</td></tr>
							<!--tr><td><h3><a href="" ></a></h3></td></tr-->
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
