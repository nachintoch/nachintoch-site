/*
 * Script para la creación de la base de gatos del sitio.
 * Author: Manuel "Nachintoch" Castillo, contact@nachintoch.mx
 *
 * Run as root database user!
 *
 * Version 1.0, january 2016
 * Since nachintoch.mx 0.1, january 2016
 */
CREATE USER 'donkeykong'@'localhost' IDENTIFIED BY 'its_a_secret_to_everybody';

CREATE DATABASE nachinto_geek_calendar;

GRANT CREATE ON nachinto_geek_calendar.* TO 'donkeykong'@'localhost';
GRANT DROP ON nachinto_geek_calendar.* TO 'donkeykong'@'localhost';
GRANT DELETE ON nachinto_geek_calendar.* TO 'donkeykong'@'localhost';
GRANT INSERT ON nachinto_geek_calendar.* TO 'donkeykong'@'localhost';
GRANT SELECT ON nachinto_geek_calendar.* TO 'donkeykong'@'localhost';
GRANT CREATE ON nachinto_geek_calendar.* TO 'donkeykong'@'localhost';

FLUSH PRIVILEGES;
