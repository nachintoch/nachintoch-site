<?
/*
 * Página de inicio del sitio.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Nachintoch Juegos</title>
		<meta name="description" content="Jugos por Nachintoch" />
		<meta name="keywords" content="juegos, android, open, source, videojuegos, apps" />
		<? require_once 'templates/header.php'; ?>
		<style >
			td {
				vertical-align: middle;
				text-align: justify;;
			}
		</style>
	</head>
	<body class="landing">
		<!-- Header -->
		<? require_once 'templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Juegos por Nachintoch</h2>
					</header>
					<!-- Content -->
						<section id="content">
							<table >
								<tr>
									<td width="10%" >
										<img height="64" width="64"
											src="https://lh3.googleusercontent.com/IHrdHsXNe56AgU1DxzOe19Re0b0XS5vlIMXneu6ydATavmPIt4zNClTe_o_nPwDgmA=s180-rw" />
									</td>
									<td width="30%" >
										<h3><a href="https://play.google.com/store/apps/details?id=com.mx.nachintoch.laberintodemovimiento" >
											S&uacute;per Laberinto de Movimiento</a></h3>
									</td>
									<td width="60%" >
										Juego de destreza que consiste en diez laberintos de dificultad progesiva.
										<br/>
										Debes llevar dos pelotas hacia sus respectivas "metas" en el centro de los
										laberintos. Para mover las pelotas debes usar tu dispositivo Android en
										posición paralela al piso y moverlo como si las pelotas rodaran sobre la
										pantalla.
									</td>
								</tr>
							</table>
						</section>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html>
