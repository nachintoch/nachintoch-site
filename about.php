<!DOCTYPE HTML>
<?
/**
 * Página de acerca de.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, january 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<html>
	<head>
		<title>Nachintoch - Acerca de</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Acerca de Nachintoch - Quien soy, que ofrece nachintoch.mx y c&oacute;mo colaborar" />
		<meta name="keywords" content="info, informaci&oacute;n, acerca, sobre, qu&eacute;" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php'; ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Acerca de Nachintoch</h2>
						<p>El l&iacute;mite es la imaginaci&oacute;n</p>
					</header>
					<!-- Content -->
						<section id="content">
							<h3>&iquest;Qu&eacute; es Nachintoch.mx?</h3>
							<p>Nachintoch.mx es el sitio web donde pretendo crear &iacute;ndices y hospedaje de recursos
							para los diversos proyectos que he realizado y participado; y otros en los que todav&iacute;a
							no me involucro.</p>
							<p>Nachintoch es el pseud&oacute;nimo que uso como desarrollador, nombre de usuario e intento
							de divulgador. Mi nombre es Manuel Ignacio Castillo y las computadoras me apasionan desde que
							ten&iacute;a 4 a&ntilde;os y empec&eacute; a jugar Zelda (A Link to the Past por si alguien
							ten&iacute;a la inquietud). No entend&iacute;a mucho de lo que pasaba, pero me maravillaba
							el hecho de que una peque&ntilde;a cajita de pl&aacute;stico tomaba un poco de electricidad
							y la transformaba en una historia fant&aacute;stica en la que t&uacute; eras el protagonista;
							no era como una pelicula donde todo pasa de forma predeterminada.</p>
							<p>Desde peque&ntilde;o descubr&iacute; mis dos grandes pasiones de toda la vida: las
							computadoras y los videojuegos.</p>
							<p>Con el tiempo descubr&iacute; que ten&iacute;a una tercer pasi&oacute;n; que siempre estuvo
							all&iacute;, resonando oculta entre las otras dos. Pero por distintas circunstancias
							extra&ntilde;as (muy, muy extra&ntilde;as) de la vida, no hab&iacute;a tenido la oportunidad de
							darme cuenta de lo mucho que me gusta aprender y entender la naturaleza del universo en el que
							existimos y la raz&oacute;n de ser de las cosas en el mundo.</p>
							<p>De esas circuntancias extrañas de la vida que me impedian desarrollar mis cualidades
							nerdas, aprend&iacute; muchas cosas, que por un lado me mantienen constantemente molesto
							por el hecho de que por el ego de unos, hay muchos que tienen que soportar distintos tipos,
							formas, colores y sabores de tempestades; pero por otro lado me queda la experiencia de que
							podemos hacer muchas cosas para que uno mismo y/o los dem&aacute;s; pese a las distintas
							situaciones dif&iacute;ciles de superar que puedan estar pasando, puedan descubrir y
							desarrollar sus habilidades, inspirarse para encontrar la fuerza y voluntad para desarrollar
							cualquiera que sean sus intenciones (por supuesto siempre que sus intenciones no agredan
							en ninguna forma a otra persona).</p>
							<p>Creo firmemente que todos tenemos un derecho superior a ser libres; libres en todo sentido,
							siempre que eso no implique afectar a alguien más.</p>
							<p>Mi intenci&oacute;n con nachintoch.mx es aportar un granito de arena en ese compromiso
							personal: ofrecer un portal p&uacute;blico sin ning&uacute;n fin monetario que concentre
							tutoriales, gu&iacute;as, consejos y otros recursos principalmente orientados a temas en
							computaci&oacute;n en todos los niveles, tratando que el contenido sea claro para cualquier
							persona (pero depende mucho del tema en cuesti&oacute;n).</p>
							<p>Si por cualquier raz&oacute;n t&uacute; adem&aacute;s de aprovechar los recursos aqu&iacute;
							hospedados, tambi&eacute;n quieres contribuir al software libre y a la comunidad y no estas
							seguro de como hacerlo, me orezco como apoyo para cualquiera interesado en contribuir al
							software libre. Colaborar al software libre es mucho m&aacute;s que compartir resultados,
							los esfuerzos por mejorar y participar con equipos de desarrollo en todo el mundo ofrece
							el desarrollo de habilidades y experiencias que pueden ser dif&iacute;ciles de desarrollar
							o mantener solo con experiencia laboral (dependiendo d&oacute;nde o c&oacute;mo trabajes, pero
							en la mayor&iacute;a de los casos se trata de desarrollar tareas monotonas muy mal
							administradas).<br/>
							Soy muy entusiasta de esta idea de crear, compartir y mejorar entre todos. Es un concepto que
							debería salir m&aacute;s all&aacute; de la computaci&oacute;n y permear el mundo en su
							totalidad. Pero; entre otras cosas que nos ense&ntilde;o Futurama, es que la humanidad solo
							cooperaria de forma &iacute;ntegra si descubrieramos lo insignificante
							que es nuestra civilizaci&oacute;n frente a otras civilizaciones (alien&iacute;genas).
							As&iacute; que al menos por ahora, el Open Source es una bonita forma de crecer entre todos.</p>
							<p>Como dice la &uacute;ltima im&aacute;gen del &iacute;ndice del sitio: <i>"The world is open
							source"</i>. Esta es una expresi&oacute;n que me gustar&iacute;a se hiciera realidad. El universo
							es de todos, y el conocimiento que nos permite aprender de &eacute;l, debe ser de f&aacute;cil
							acceso y se debe propiciar su aprendizaje.</p>
							<p>El prop&oacute;sito de la tecnolog&iacute;a es ayudarnos, hacernos el d&iacute;a a d&iacute;a
							m&aacute;s f&aacute;cil para preocuparnos por las cosas importantes que no pueden hacer las
							m&aacute;quinas. Hagamos que esto sea cierto, y hag&aacute;moslo nostros mismos: no esperemos a
							que todo nos llegue del cielo, porque no lo har&aacute;. No importa para que seas bueno (o para
							qu&eacute; te hagas bueno &#128540; ), el futuro necesita y depende de todos.</p>
							<p>Recuerden que <i><b>el l&iacute;mite es la imaginaci&oacute;n</b></i>.</p>
						</section>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php'; ?>
	</body>
</html>
