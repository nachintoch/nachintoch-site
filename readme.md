## Descripción
[Algún día, quizá en dentro de los próximos 10 años, agregue aquí una
descripción]

## Installation
Esta página Web está diseñada para operar con el plugin PHP del servidor Web
Apache 2.


1. Prepare el entorno anfitrión del servidor Web (prepare un servidor, una
máquina critua, etc...)
2. Copie los archivos del repositorio en el directorio raíz asignado al servidor
Web Apach 2.


*Asegúrese* de copiar los archivos ocultos .htaccess en el entorno de
ejecicuión.


4. Instale PHP v >= 6. En Debian 9 ejecute:
```bash
# apt install php7.0 mysql-server libapache2-mod-php php-mysql
```
3. Habilite el plugin PHP para Apache 2:
```bash
# a2enmod php7.0
```
4. Habilita las etiquetas de PHP abreviadas. En Debian 9 edite
`/etc/php/7.0/apache2/php.ini` y busque la línea no comentada que inicia con
`short_open_tag` y editela como sigue:
```
short_open_tag = On
```
También se requiere la extensión de PHP `mysqli`.

5. Habilite Rewrite Engine:
```bash
# a2enmod rewrite
```

6. Habilite los archivos .htaccess en el sitio virtual de Apache 2 configurado
para hospedar la página Web:
```bash
<VirtualHost *:80>
    ...
    <Directory />
        ...
        AllowOverride All
    </Directory>
</VirtualHost>
```

7. Habilite el plugin para encabezados de Apache 2:
```bash
# a2enmod headers
```
8. Reinicie el servicio de Apache 2.
```bash
# service apache2 restart
```
9. Ejecute los scripts SQL en el directorio `db`.
10. Pruebe el lanzamiento.
