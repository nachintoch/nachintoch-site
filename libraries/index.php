<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Nachintoch - Bibliotecas de software libre usa y/o modifica de acuerdo a tus necesidades y gustos" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, android, c, haskell, java, posix, linux, mac" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas</h2>
						<p>Recursos para desarolladores</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<h3>Muchos tenemos ese m&aacute;gico programa que hemos reusado una y otra vez por su gran utilidad</h3>
							<p>Y yo quiero compartir con todos ustedes los m&iacute;os</p>
							<p>Por favor, si usas las bibliotecas que aqu&iacute; comparto, te pido que des cr&eacute;dito a mi
							trabajo en donde la uses. Tanto en el c&oacute;digo fuente como en alguna parte de tu producto en tiempo
							de ejecuci&oacute;n.<br/>
							Si quisieras, tambi&eacute;n puedes hacermelo saber (<a href="mailto:contact@nachintoch.mx" >escribi&eacute;ndome ;)</a>)
							y puedo abrir una lista en con enlaces de referencia a tus sitios o publicaciones. &iexcl;crezcamos juntos!</p>
							<p>A continuaci&oacute;n, listo las plataformas para las que dispongo y comparto algunas utiler&iacute;as.</p>
							<table><tbody><tr><td><h3><a href="android" >Android</a></h3></td></tr>
								<tr><td><h3><a href="C" >C y C++</a></h3></td></tr>
								<tr><td><h3><a href="haskell" >Haskell</a></h3></td></tr>
								<tr><td><h3><a href="java" >Java</a></h3></td></tr>
								<tr><td><h3><a href="python" >Python</a></h3></td></tr>
								<tr><td><h3><a href="posix" >Consola de Linux (algunos son compatibles con Mac por ser sistemas POSIX-compatibles)</a></h3><td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>
