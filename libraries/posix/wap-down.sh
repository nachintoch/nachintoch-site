#!/bin/bash
ifconfig wlan1 down
iptables --flush
iptables --table nat --flush
iptables --delete-chain
iptables --table nat --delete-chain
sysctl -w net.ipv4.ip_forward=0
service hostapd stop
service isc-dhcp-server stop
service networking restart
service network-manager start
