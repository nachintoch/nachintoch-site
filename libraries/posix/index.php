<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, posix, linux, mac" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas para <b>sistemas basados en UNIX</b></h2>
						<p>La estrecha l&iacute;nea entre los desarrolladores y los hipsters</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Cuando descubr&iacute; Linux y empec&eacute; a usarlo con frecuencia, sent&iacute; que toda mi vida
							hab&iacute;a sido una mentira. Desde entonces, salvo que sea realmente necesario, no uso otro sistemas
							operativos que no est&eacute;n basados en Linux. Particularmente, uso Debian (y Android considerando que
							el kernel es Linux :D )</p>
							<p>En consecuencia, tengo algunos scripts que pueden ayudarnos a resolver uno que otro problema o simplemente
							hacer la vida m&aacute;s f&aacute;cil.</p>
							<table><tbody><tr><td><h4><a href="backup-cdrom.sh" >Respalda un disco &oacute;ptico en un archivo *.ISO</a></h4>
								Por diversos motivos, puede ser muy &uacute;til copiar un CD (de cualquier tipo: DVD,
								bluray, etc...) en un archivo ISO; que podemos quemar f&aacute;cilmente. Todo buen usuario de discos
								&oacute;pticos, sabe que tener varios procesos corriendo mientras se opera con un disco, aumenta la probabilidad
								de errores, lo que puede implicar tener que desechar discos y volver a 
								esperar por la siguiente copia (cuando hacemos quemado). Usar un programa con GUI para estos prop&oacute;sitos, aumenta por muy muy poco
								esa misma probabilidad, ya que recordemos que las interfaces gr&aacute;ficas son pesadas.<br/>
								Por otro lado, muchos servidores que ejecutan linux no suelen levantar la interfaz gr&aacute;fica. Por estos
								motivos y otros que no se me ocurren en este momento, tengo este script con el que podemos hacer la copia de
								un CD a ISO desde la consola. Un hecho, es que la copia se realiza mucho m&aacute;s r&aacute;pido que si la
								hici&eacute;ramos con una GUI.</td></tr>
								<tr><td><h4><a href="hp_printers_drivers_installer.zip" >Instalador de drivers para impresoras HP de la serie 1000</a></h4>
								Una de las grandes desventajas de usar Linux la mayor parte del tiempo, es que es com&uacute;n
								encontrarnos con dispositivos para los que no tenemos los drivers, y en consecuencia; muchas veces
								no podemos usarlos con nuestro sistema Linux. La serie de impresoras HP Laserjet 1000 es un caso especial,
								ya que si existen los drivers, pero puede ser un poco truculento lograr hacerlos funcionar. En alg&uacute;n
								momento tuve este problema y mi mejor soluci&oacute;n se encuentra en este par de scripts que instalan los
								drivers... Aunque no puedo garantizar &eacute;xito (a veces funciona bien, a veces no; hay que tener cuidado
								al revisar las dependencias de nuestro sistema)</td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>