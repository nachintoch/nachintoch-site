#!/bin/bash
service network-manager stop
ifconfig wlan1 10.5.5.1 netmask 255.255.255.0 up
service networking restart
service isc-dhcp-server restart
iptables --flush
iptables --table nat --flush
iptables --delete-chain
iptables --table nat --delete-chain
iptables --table nat --append POSTROUTING --out-interface eth0 -j MASQUERADE -v
iptables --append FORWARD --in-interface wlan1 -j ACCEPT -v
sysctl -w net.ipv4.ip_forward=1
service hostapd restart
