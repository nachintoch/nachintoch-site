#!/bin/bash

# Crea un ISO basado en el disco en cdrom y que va a un fichero de nombre
# dado al llamar al script. Puede darse una ruta relativa o absoluta también.
# autor: Manuel Castillo "Nachintoch"
# version: 1.0, enero 2015
if [ $# -eq 0 ]
then
	echo "No iso name or path supplied to script";
	exit;
fi
isoinfo -d -i /dev/cdrom | grep -i -E 'block size|volume size';
dd if=/dev/cdrom of=$1 #bs=block size count=volume size
echo "CD copied to $1";
exit;

