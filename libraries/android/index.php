<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, android, c, java" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas para <b>Android</b></h2>
						<p>iWey</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Hasta ahora, Android es mi plataforma predilecta: tanto como usuario como desarrollador.<br/>
							Por ahora, solo puedo compartirles lo que llamo la <i>biblioteca para escucha de sensores</i>.
							Como su nombre indica, contiene "pre-implementaciones" de las interfaces que definen sensores
							de Android, lo que puede resultar &uacute;til para aplicaciones en las que nos interesa poco
							interactuar con el sensor y solo recabar la informaci&oacute;n que lee.</p>
							<p>Prometo hacer crecer esta lista con el tiempo, tengo otros varios proyectos para bibliotecas
							de Android, pero algunos est&aacute;n incompletos y otros ni los he empezado... Pero los terminar&eacute;
							eventualmente.</p>
							<table><tbody><tr><td><h3>Escucha de sensores</h3><br/>
								<ul><li><a href="sensor_listener_lib_v1.1.jar" >Biblioteca Jar</a></li>
								<li><a href="https://github.com/Nachintoch/android-sensor-listener" target="_blank" >
									C&oacute;digo Fuente</a></li></ul></td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>