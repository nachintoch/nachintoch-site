<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, c" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas para <b>C y C++</b></h2>
						<p>printf("Hay que sangr\u00F3n");</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>C es mi lenguaje de programaci&oacute;n favorito... Por su sintaxis, por su flexibilidad, por permitir
							manipular de manera tan abierta los dispositivos... Por muchos motivos. Desgraciadamente; para mi suerte,
							no he participado en grandes proyectos donde se ocupe C, por lo que lo que aqu&iacute; les comparto, es
							b&aacute;sicamente el resultado de crear aplicaciones en C en mi tiempo libre o perfeccionamiento de
							algunas tareas.</p>
							<table><tbody><tr><td><h4><a href="create_project_v1.0.zip" >Scripts para crear y manejar un proyecto en C</a></h4>
									Scripts que generan una serie de directorios que satisfacen la convenci&oacute;n de desarrollo en C. Me han
									sido muy &uacute;tiles para iniciar cualquier trabajo. (Nunca me ha gustado trabajar con IDEs ni con frameworks,
									me gusta m&aacute;s hacer este tipo de cosas)</td></tr>
								<tr><td><h4><a href="https://github.com/Nachintoch/ejemplos-concurrencia/tree/master" target="_blank"
										title="github" >Ejemplos de resoluci&oacute;n al problema del consenso</a></h4>
									Este podr&iacute;a ir tambi&eacute;n en la parte de c&oacute;digo y bibliotecas para Java, pero la
									soluci&oacute;n propuesta para C es mucho m&aacute;s interesante. Sobre todo por el detalle de que
									me di cuenta a la mala, de que no podemos usar registros como variables at&oacute;micas, por
									la cuesti&oacute;n de los cambios de contexto de un programa durante su ejecuci&oacute;n. Para obtener
									m&aacute;s informaci&oacute;n sobre lo anterior, les recomiendo consultar <a
										href="https://nachintoch.wordpress.com/2015/11/28/computo-concurrente-ejemplos-de-solcuion-al-problema-del-consenso-en-java-y-c-uso-de-las-herramientas-de-concurrencia/"
										target="_blank" title="wordpress">el post que hice en mi blog sobre este c&oacute;digo</a>
								</td></tr>
								<tr><td><h4><a href="/teaching/concurrent_computing/anexos_c.zip" >Herramientas de concurrencia y
									sincronizaci&oacute;n en C y C++.</a></h4>
									Este es el anexo a unas notas de clase sobre concurrencia y sincronizaci&oacute;n en C. Para obtener
									m&aacute; informacici&oacute;n recomiendo consultar <a
										href="/teaching/concurrent_computing/Concurrencia_sincronizacion_C.pdf" >las notas</a>
								</td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>