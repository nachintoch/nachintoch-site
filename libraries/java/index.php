<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, android, java, sprite" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas para <b>Java</b></h2>
						<p>Podr&iacute;s usarlas hasta en un horno de microondas</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>No lo voy a negar: me gusta programar en Java y siendo francos, la mayor&iacute;a de los argumentos por
							los que se intenta exhibir que no es tan buen lenguaje como otros: son falsos en la mayor&iacute;a de los
							casos (porque es "lento" y "feo" y provoca c&aacute;ncer y no se que otras cosas se inventan...)</p>
							<p>La mayor&iacute;a de los proyectos en los que he participado han sido hechos en Java, adem&aacute;s de
							que me encanta trabajar en Android y resulta que el lenguaje de desarrollo nativo es Java, estas bibliotecas
							tambi&eacute;n pueden ser usadas en proyectos Android (usando el entorno nativo). Di no a los entornos de
							desarrollo paralelos.</p>
							<table><tbody><tr><td><h4><a href="nachintoch_library.zip" >Biblioteca de utilidades de Nachintoch</a></h4>
									Esta es una serie de paquetes en Java que contiene implementaciones de: monitores
									(c&oacute;mputo concurrente), "extensiones" de los envolventes de tipos de datos de Java; &uacute;tiles
									para su uso con estructuras de datos u ordenamientos que requieren de tipos comparables, modelo de
									vectores y modelo de Sprite con hojas de animaci&oacute;n en 2D y resoluci&oacute;n de cuestiones
									f&iacute;sicas como movimiento, fricci&oacute;n e interacci&oacute;n con otros Sprites.<br/>
									Cabe hacer menci&oacute;n de que Java ya tiene implementaciones de algunas funcionalidades de mi biblioteca y suelen
									ser m&aacute;s eficientes. Este tipo de clases las he conservado en el ZIP por su car&aacute;cter did&aacute;ctico.</td></tr>
								<tr><td><h4><a href="../../teaching/intro_data_structure/ejemplos_XML.zip" >Ejemplos de parseo y uso de XML con Java</a></h4>
									Este es un ejemplo de una de mis notas de clase, donde se habla precisamente sobre c&oacute;mo leer un archivo XML usando:
									SAX, DOM, JDOM y StAX.
								</td></tr>
								<tr><td><h4><a href="../../teaching/intro_data_structure/ejemplosIO.zip" >Ejemplos de entrada y salida de texto en Java</a></h4>
									Este es un ejemplo de una de mis notas de clase, donde extiendo lo visto en el manejo y parseo de XML para ahora leer y
									escribir archivos de texto de toda naturaleza; incluyendo la modificaci&oacute;n y creaci&oacute;n de archivos XML usando
									SAX, DOM, JDOM y StAX.
								</td></tr>
								<tr><td><h4><a href="../../teaching/intro_data_structure/ejemplo_recursion.zip" >Ejemplo de recursi&oacute;n y benchmarks</a></h4>
									Este es un ejemplo de una de mis notas de clase, donde ejemplifico la recursi&oacute;n en Java y adem&aacute;s se hacen benchmarks
									a las ejecuciones de la recursi&oacute;n.
								</tr></td>
								<tr><td><h4><a href="../../teaching/algorithm_analisys/max_con_seq/MCS.java" >
									Soluci&oacute;n al problema de la subsecuencia m&aacute;xima consecutiva</a></h4>
									Esta clase, resuelve el problema de la m&aacute;xima subsecuecia consecutiva.
								</td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>