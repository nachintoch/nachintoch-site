{- Contiene funciones para utilizar como herramientas generales para simplificar
   el uso de otras funciones más específicas -}
module Tools where

-- Muestra como cadena los elementos de un conjunto.
showSet :: (Show a) => [a] -> String
showSet [] = ""
showSet (x: xs) = if (length xs) > 0
    then (show x) ++"," ++(showSet xs)
    else show x
    
-- Indica si un elemento dado pertenece a un conjunto dado.
isIn :: (Eq a) => a -> [a] -> Bool
isIn a [] = False
isIn a (x: xs) = if a == x then True else isIn a xs

-- Indica si dos listas son equivalentes
areEqual :: (Eq a) => [a] -> [a] -> Bool
areEqual [] [] = True
areEqual a [] = False
areEqual [] b = False
areEqual (x :xs) (y :ys) = if (length (x :xs)) == (length (y :ys)) then
    if x == y then areEqual xs ys else False
    else False

-- Muestra como cadena los elementos de una relación
showRelation :: (Show a, Show b) => [a] -> [b] -> String
showRelation [] [] = "" --La cardinalidad de las listas debe ser la misma
showRelation (a :ax) (b :bx) = "(" ++(show a) ++"," ++(show b) ++"),"
    ++(showRelation ax bx)

thr :: (a,b,c) -> c
thr (a,b,c) = c

scnd :: (a,b,c) -> b
scnd (a,b,c) = b

frst :: (a,b,c) -> a
frst (a,b,c) = a

