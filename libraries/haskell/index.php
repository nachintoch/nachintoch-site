<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, haskell" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas para <b>Haskell</b></h2>
						<p>lambda nosequeponer</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Tengo una relaci&oacute;n amor odio con Haskell. Los lenguajes funcionales nunca han sido lo
							m&iacute;o realmente; tampoco los interpretados. Pero Haskell es una mezcla de elegancia matem&aacute;tica
							y simpleza y utilidad, por lo que no tengo nada en contra de &eacute;l</p>
							<p>Hasta ahora, &uacute;nicamente he usado Haskell para tareas de la licenciatura y en alg&uacute;n momento
							hice esta mini-mini biblioteca con algunas funciones que me resultaron muy &uacute;tiles en su momento.
							La publico aqu&iacute; con la esperanza de que a alguien le sea de utilidad.</p>
							<table><tbody><tr><td><h4><a href="Tools.hs" >Herramientas para con Haskell</a></h4></td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>