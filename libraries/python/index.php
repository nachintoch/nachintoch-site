<?
/*
 * Página de bibliotecas.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Bibliotecas - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="codigo, open, source, software, libre, programacion, c" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>C&oacute;digo y bibliotecas para <b>Python</b></h2>
						<p>print("20 pesos para Slytherin")</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<p>Python, es... un bonito lenguaje de programaci&oacute;n. No tengo mucho que decir sobre Python;
							m&aacute;s que me gusta y es un bonito y buen lenguaje de programaci&oacute;n. En realidad nunca lo
							he usado fuera de la academia y por lo mismo no he tenido tantas experiencias como para tenerle
							alg&uacute;n cari&ntilde;o en especial; como pasa con otros lenguajes... Ojo; que yo no lo haya usado
							fuera de la academia no significa que no sea un lenguaje usado o popular en la industria: lo es y
							bastanta; pero yo simplemente nunca he tenido la oportunidad o inter&eacute;s de trabajar en esos
							proyectos.</p>
							<table><tbody>
								<tr><td><h4><a href="/teaching/concurrent_computing/ejemplos_python.zip" >Herramientas
									de concurrencia y sincronizaci&oacute;n en Python</a></h4>
									Anexo de notas; donde se ejemplifica el uso de las bibliotecas est&aacute;ndares de Python
									para resolver problemas que invlucren concurrencia/sincroniaci&oacute;n. Para m&aacute;s
									informaci&oacute;n dejo el <a
										href="/teaching/concurrent_computing/Concurrencia_sincronizacion_Python.pdf" >enlace a
										las notas</a>
								</td></tr>
							</tbody></table>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>