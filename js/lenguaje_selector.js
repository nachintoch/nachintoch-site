/* 
 * Script que carga el lenguaje de prograación seleccionado.
 * author: Manuel "Nachintoch" Castillo, contact@nachintoch.mx
 * version 1.0, march 2017
 * since Nachintoch.mx 1.0, march 2017
 */

// recuerda el contenedor de código
var container = document.getElementById("display");
// recuerda al selector de lenguajes
var selector = document.getElementById("leng-sel");
// recuerda el proyecto
var proyect = document.getElementById("proyect").value;

// asigna el lenguaje por defecto
load_lang();

// asigna el receptor de eventos del selector
selector.addEventListener("change", load_lang, false);

/*
 * Carga el lenguaje de programación deseado.
 * returns undefined
 */
function load_lang() {
	var lang = selector.value;
	container.innerHTML = '<object type="text/html" data="' +proyect +'/' +lang +'.php" style="width:100%;height:100%;background:#afafaf;border-radius:25px;padding:20px" ></object>';
}//load_lang
