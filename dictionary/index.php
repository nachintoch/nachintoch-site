<?
/*
 * Página del diccionario.
 * @author Manuel "Nachintoch" Castillo contact@nachintoch.mx
 * @version 1.0, march 2016
 * @since Nachintoch.mx 1.0, january 2016
 */
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Diccionario - Nachintoch Desarrollos</title>
		<meta name="description" content="Bibliotecas de software" />
		<meta name="keywords" content="diccionario, referencias, significados, denotacion, connotacion, computacion, matematicas, ciencias, geek" />
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/header.php'; ?>
		<style >
			/* CSS para lograr el efecto de las pestañas.
			 Creado por inspirationalpixels.com */
		
			/*----- Tabs -----*/
			.tabs {
				width:100%;
				display:inline-block;
			}

			/*----- Tab Links -----*/
			/* Clearfix */
			.tab-links:after {
				display:block;
				clear:both;
				content:'';
			}

			.tab-links li {
				margin:0px 5px;
				float:left;
				list-style:none;
			}

			.tab-links a {
				padding:9px 15px;
				display:inline-block;
				border-radius:3px 3px 0px 0px;
				font-size:16px;
				font-weight:600;
				transition:all linear 0.15s;
			}

			.tab-links a:hover {
				background:#4C4C4C;
				text-decoration:none;
			}

			li.active a, li.active a:hover {
				background:#fff;
				color:#4c4c4c;
			}

			/*----- Content of Tabs -----*/
			.tab-content {
				padding:15px;
				border-radius:3px;
				box-shadow:-1px 1px 1px rgba(0,0,0,0.15);
			}

			.tab {
				display:none;
			}

			.tab.active {
				display:block;
			}
		</style>
		<script type="text/javascript" >
			jQuery(document).ready(function() {
				jQuery('.tabs .tab-links a').on('click', function(e)  {
					var currentAttrValue = jQuery(this).attr('href');

					// Show/Hide Tabs
					jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

					// Change/remove current tab to active
					jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

					e.preventDefault();
				});
			});
		</script>
	</head>
	<body>
		<!-- Header -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/menu.php' ?>
		<!-- Main -->
			<div id="main" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Diccionario Nachintoch</h2>
						<p>Computaci&oacute;n | Matem&aacute;ticas | Ciencia | Cultura geek</p>
					</header>
					<div class="row 150%">
						<section id="content">
							<div class="tabs" >
								<ul class="tab-links" ><li class="active" ><a href="#a" >A</a></li>
								<li><a href="#b" >B</a></li>
								<li><a href="#c" >C</a></li>
								<li><a href="#d" >D</a></li>
								<li><a href="#e" >E</a></li>
								<li><a href="#f" >F</a></li>
								<li><a href="#g" >G</a></li>
								<li><a href="#h" >H</a></li>
								<li><a href="#i" >I</a></li>
								<li><a href="#j" >J</a></li>
								<li><a href="#k" >K</a></li>
								<li><a href="#l" >L</a></li>
								<li><a href="#m" >M</a></li>
								<li><a href="#n" >N</a></li>
								<li><a href="#o" >O</a></li>
								<li><a href="#p" >P</a></li>
								<li><a href="#q" >Q</a></li>
								<li><a href="#r" >R</a></li>
								<li><a href="#s" >S</a></li>
								<li><a href="#t" >T</a></li>
								<li><a href="#u" >U</a></li>
								<li><a href="#v" >V</a></li>
								<li><a href="#w" >W</a></li>
								<li><a href="#x" >X</a></li>
								<li><a href="#y" >Y</a></li>
								<li><a href="#z" >Z</a></li></ul>
								<hr/>
								<div class="tab-content" >
									<div id="a" class="tab active" >
										Aqui debe ir un iFrame
									</div>
									<div id="b" class="tab" >
										Gato
									</div>
									<div id="c" class="tab" >
										C
									</div>
									<div id="d" class="tab" >
										D
									</div>
									<div id="e" class="tab" >
										E
									</div>
									<div id="f" class="tab" >
										F
									</div>
									<div id="g" class="tab" >
										G
									</div>
									<div id="h" class="tab" >
										H
									</div>
									<div id="i" class="tab" >
										I
									</div>
									<div id="j" class="tab" >
										J
									</div>
									<div id="k" class="tab" >
										K
									</div>
									<div id="l" class="tab" >
										L
									</div>
									<div id="m" class="tab" >
										M
									</div>
									<div id="n" class="tab" >
										N
									</div>
									<div id="o" class="tab" >
										O
									</div>
									<div id="p" class="tab" >
										P
									</div>
									<div id="q" class="tab" >
										Q
									</div>
									<div id="r" class="tab" >
										R
									</div>
									<div id="s" class="tab" >
										S
									</div>
									<div id="t" class="tab" >
										T
									</div>
									<div id="u" class="tab" >
										U
									</div>
									<div id="v" class="tab" >
										V
									</div>
									<div id="w" class="tab" >
										W
									</div>
									<div id="x" class="tab" >
										X
									</div>
									<div id="y" class="tab" >
										Y
									</div>
									<div id="z" class="tab" >
										Z
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		<!-- Footer -->
		<? require_once $_SERVER["DOCUMENT_ROOT"] .'/templates/footer.php' ?>
	</body>
</html>